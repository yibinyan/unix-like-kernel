/**
 * @file cond.c
 *
 * @brief This file contains functions related to Conditional Varaiable
 *
 * @author Yibin Yan
 */
#include <sem_type.h>
#include <syscall.h>
#include <stdlib.h>
#include <simics.h>

extern int is_alive(int tid);

/**
 * @brief Initialize the semaphore pointed to by sem to the value count
 *
 * @param sem Semaphore structure
 * @count value of semaphora
 *
 * @return void
 */
int sem_init(sem_t *sem, int count){
   
    sem->count = count;
    list_init(&(sem->wait_list));
       
    /* initialize mutex */
    if (mutex_init(&(sem->sem_mutex)) < 0){
        return -1;  /* Fail to initialize mutex, return -1*/
     }else{
        /* success */
        return 0;
    }
}

/**
 * @brief Destroy semaphore
 *
 * @param sem Semaphore structure
 *
 * @return void
 */
void sem_destroy(sem_t *sem){
    if (is_empty(&sem->wait_list)){
        mutex_destroy(&(sem->sem_mutex));    
        list_destroy(&(sem->wait_list));
    }else {
        /* illegal to destroy a samephora while threads are waiting on it*/
        exit(-1);
    }
}

/**
 * @brief Allow a thread to decrement a semaphore value, and may cause block
 *
 * @param sem Semaphore structure
 *
 * @return void
 */
void sem_wait(sem_t *sem){

    mutex_lock(&(sem->sem_mutex));
    sem->count--;
    if (sem->count < 0){
        node_t *waitlist_node = (node_t *)malloc(sizeof(node_t));
        waitlist_node->value = (void *)gettid();
        append(&(sem->wait_list), waitlist_node);
         int reject = 0;
        mutex_unlock(&(sem->sem_mutex));
        deschedule(&reject);
    }else{
        mutex_unlock(&(sem->sem_mutex));
    }

}

/**
 * @brief Wake up a thread waiting on the semaphore, update semaphora value
 *
 * @param sem Semaphore structure
 *
 * @return void
 */
void sem_signal(sem_t *sem){

    mutex_lock(&(sem->sem_mutex));
    sem->count++;

    int tid;
    node_t *node;
    if (sem->count <= 0) {
        if ((node = pop(&(sem->wait_list))) != NULL){
            tid = (int)node->value;
    
            if (make_runnable(tid) < 0){
                /* Thread is still on schedule, 
                 * append to list, decrease sem  and signal again */
                append(&(sem->wait_list), node);
                sem->count--;
                mutex_unlock(&(sem->sem_mutex));
                yield(-1); 
                sem_signal(sem); 
                return;
            }else{
                free(node);
            }
        }
    }
        
    mutex_unlock(&(sem->sem_mutex));
}

