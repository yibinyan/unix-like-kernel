/**
 * @file malloc.c
 *
 * @brief thread safe malloc
 * We use spin lock to guard each function call
 *
 */
#include <malloc.h>
#include <stdlib.h>
#include <mutex.h>

static spin_t lock ={1};

void *malloc(size_t __size)
{
  void *result = NULL;
  spin_acquire(&lock);
  result = _malloc(__size);
  spin_release(&lock);

  return result;
}

void *calloc(size_t __nelt, size_t __eltsize)
{

  void *result = NULL;
  spin_acquire(&lock);
  result = _calloc(__nelt,__eltsize);
  spin_release(&lock);

  return result;
}

void *realloc(void *__buf, size_t __new_size)
{
  void *result = NULL;
  spin_acquire(&lock);
  result = _realloc(__buf,__new_size);
  spin_release(&lock);
  return result;
}

void free(void *__buf)
{
  spin_acquire(&lock);
  _free(__buf);
  spin_release(&lock);
}


