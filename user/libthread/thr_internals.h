/** @file thr_internals.h
 *
 *  @brief This file may be used to define things
 *         internal to the thread library.
 */



#ifndef THR_INTERNALS_H
#define THR_INTERNALS_H

#include <sem.h>

/** 
 * @brief Structure used to store stack elements used by
 * one thread
 */
typedef struct stack_el{
	/** @brief the low address of the allocated stack */
    void *bottom;
}stack_el_t;

/**
 *	@brief Structure used to store thread infomation by
 *	the thread library
 */
typedef struct thread{
	/** @brief the thread id */
	int tid;
	/** @brief The counter of how many threads have been joined */
	int join_count;
	/** @brief The thread id which joins to this thread */
	int joiner_tid;
	/** @brief The allocated stack of the thread */
	stack_el_t *stack;
	/** @brief We use semaphore to notify the joiner thread when exiting */
	sem_t sem;
	/** @brief Variable to store the exit code */
	int exit_value;
	/** @brief Whether this thread has exited */
	int exited;
} thread_t;

#endif /* THR_INTERNALS_H */
