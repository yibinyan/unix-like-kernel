#include <rwlock_type.h>
#include <syscall.h>
#include <mutex.h>
#include <cond.h>
#include <rwlock.h>
#include <stdlib.h>
#include <simics.h>
int rwlock_init(rwlock_t *rwlock){
    rwlock->running_reader = 0;
    rwlock->waiting_reader = 0;
    rwlock->running_writer = 0;
    rwlock->waiting_writer = 0;
    
    mutex_init(&rwlock->rw_mutex);
    cond_init(&rwlock->r_cond);
    cond_init(&rwlock->w_cond);
    
    return 0;
}

void rwlock_destroy(rwlock_t *rwlock){
    cond_destroy(&rwlock->r_cond);
    cond_destroy(&rwlock->w_cond);
    mutex_destroy(&rwlock->rw_mutex);
}

void rwlock_lock(rwlock_t *rwlock, int type){
    mutex_lock(&rwlock->rw_mutex);
    
    if (type == RWLOCK_READ){
        /* if lock type is reader lock  */
        rwlock->waiting_reader++;   /* increase number of reader in waiting*/
        while ((rwlock->running_writer > 0) 
                    || (rwlock->waiting_writer > 0)){
          /* if there are writers waiting or runnning, always favor writer */
            cond_wait(&rwlock->r_cond, &rwlock->rw_mutex);
        }
    
        rwlock->waiting_reader--;
        rwlock->running_reader++;
        
    }else if (type == RWLOCK_WRITE){
        rwlock->waiting_writer++;
        while (rwlock->running_reader > 0){
           /* only wait for running reader, 
            * but run before waiting reader*/ 
            cond_wait(&rwlock->w_cond, &rwlock->rw_mutex);
        }
        
        rwlock->waiting_writer--;
        rwlock->running_writer++;
    }else {
        /* error type, exit */
        exit(-1);
    }
    
    mutex_unlock(&rwlock->rw_mutex);
}

void rwlock_unlock(rwlock_t *rwlock){
    /* We can judge the type of lock by number of running reader*/
    mutex_lock(&rwlock->rw_mutex);
    
    if ((rwlock->running_reader == 0) && (rwlock->running_writer == 0)){
        /* number of running reader and writer are both 0, no one is locked, error */
        exit(-1);
    }else if ((rwlock->running_reader > 0) && (rwlock->running_writer > 0)){
        /* number of running reader and writer are both > 0, 
         * both readers and writers are running, error */
        exit(-1);
    }else if (rwlock->running_reader == 0){
        /* Unlock writer */
        rwlock->running_writer--;
        cond_broadcast(&rwlock->w_cond);
        
        if ((rwlock->running_writer == 0) && (rwlock->waiting_writer == 0)){
            /* let reader know, only if all wrtirer is done */
            cond_broadcast(&rwlock->r_cond);
        }
    }else {
        /* Unlock reader */
        rwlock->running_reader--;
        if (rwlock->running_reader == 0){
            /* this is the last running reader, let writer know */
            if (rwlock->waiting_writer > 0){
                cond_broadcast(&rwlock->w_cond);
            }
        }
    }

    mutex_unlock(&rwlock->rw_mutex);
}


void rwlock_downgrade(rwlock_t *rwlock){
    if (rwlock->running_reader == 0){
        rwlock_unlock(rwlock);
        rwlock_lock(rwlock, RWLOCK_READ);
    }

}
