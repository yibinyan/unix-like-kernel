/** @file list.c
 *
 *  @brief A generally purpose thread-safe list.
 *  The data is stored in the list node data field
 *
 *
 */
#include <list.h>
#include <stdlib.h>
#include <mutex.h>

/**
 *  @brief init for the list.
 *  Empty list by default.
 *
 *  @param list Pointer to the list
 *
 */
void list_init(list_t *list){
	list->tail = NULL;
	list->head = NULL;
	mutex_init(&list->lock);
}

/** 
 * @brief Destory the list.
 *
 */
void list_destroy(list_t *list){
	mutex_destroy(&list->lock);
}

/**
 *  @brief Check whether a specified list is empty or not.
 *  It is used internally, we will not lock the list.
 *
 *  @param list The pointer to the list
 *
 *  @return True if the list is empty, otherwise false
 */
int _is_empty(list_t *list){
    return (list->tail==NULL) && (list->head==NULL);
}


/**
 * @brief thread safe version of _is_empty.
 *
 * @param list The pointer to the list
 *
 * @return True if the list is empty, otherwise false
 */
int is_empty(list_t *list){
	int result = 0;
	mutex_lock(&list->lock);
	result = _is_empty(list);
	mutex_unlock(&list->lock);

	return result;
}

/**
 *	@brief Return the head of the list, if the list
 *	is empty, return NULL
 *
 *	@param list The pointer to the list
 *
 *	@return The head of the list. NULL if list is
 *	empty
 */
node_t *pop(list_t* list){
	node_t *result;

	mutex_lock(&list->lock);
	
	/* If the queue is empty return NULL*/
	if(_is_empty(list)){
		result =  NULL;
	}else{
		/* There is only one element in queue */
		if(list->head==list->tail){
			result = list->head;
			list->head = NULL;
			list->tail = NULL;
		}else{
			result = list->head;
			list->head = result->next;
		}
	}
	mutex_unlock(&list->lock);
	return result;
}

/**
 *	@brief Iterate the list to find the list element which
 *	satisfy the filter function. It will return the element
 *	which first satisfy the filter.
 *
 *	@param list The pointer the searching list
 *	@param filter The function checking whether one queue
 *	element meets the filter requirement
 *	@param arg The argument to filter function
 *
 *	@return The found element. If the element is not found,
 *	return NULL
 *
 */
node_t *
find_element(list_t *list,int(*filter)(node_t*,void*),void *arg){
	node_t *iter;
	node_t *result = NULL;

	mutex_lock(&list->lock);
	iter = list->head;
	while(iter!=NULL){
		if(filter(iter,arg)){
			result = iter;
			break;
		}
		iter = iter->next;
	}
	mutex_unlock(&list->lock);

	return result;
}


/**
 *	@brief Append a list elment to the list's header.
 *
 *	@param list The list pointer
 *	@param el The adding element
 *
 *	@return Void
 */
void append(list_t *list, node_t *el){
	mutex_lock(&list->lock);

	if(_is_empty(list)){
		list->head = el;
		list->tail = el;
	}else{
		el->next = list->head;
		list->head = el;
	}

	mutex_unlock(&list->lock);
}


/**
 *	@brief Remove element from the list
 *
 *	@param list The list pointer
 *	@param ele The ele to be removed from the list
 *
 */
void remove_element(list_t *list,node_t *ele){
	mutex_lock(&list->lock);

	node_t *prev,*cur;
	cur  = list->head;

	/* Iterate the list to find a match */
	while(cur!=NULL){
		if(cur==ele){
			break;
		}

		prev = cur;
		cur = cur->next;
	}

	/* If there is a match */
	if(cur!=NULL){
		/* The removed ele is at the head */
		if(cur==list->head){
			/* There is only one element in the queue */
			if(cur->next == NULL){
				list->head = NULL;
				list->tail = NULL;
			}else{
				list->head = cur->next;
			}
		}else{
			prev->next = cur->next;
		}
		
		/* If the delete element is the tail, update
		 * the new tail
		 */
		if(cur==list->tail){
			list->tail = prev;
			list->tail = NULL;
		}

		/* free the node memory */
		free(cur);
	}
	mutex_unlock(&list->lock);
}
