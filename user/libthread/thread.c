/**
 *
 */

#include <thread.h>
#include <syscall.h>
#include <stdlib.h>
#include <simics.h>
#include <error.h>
#include <tqueue.h>
#include <mutex.h>


/* We need adjust the stack size to PAGE alligned */
#define GET_STACK_SIZE(size) (((size-1)/PAGE_SIZE+1)*PAGE_SIZE)
/* When allocating memory, the address should be page alligned */
#define PAGE_ALLIGNED_ADDR(addr) (void*)(((int)addr)/PAGE_SIZE*PAGE_SIZE)

/* External variable from autostack, we 
 * use it to allocate new thread stack
 */
extern void *stack_bottom;

/**
 * @brief Help function written in Assembly to
 * get current ebp
 *
 * @return Current ebp value.
 *
 */
extern void *get_ebp();

/*********************************************
 *			Global variable section			 *
 ********************************************/
void *thread_stack_bottom;	//Store current thread stack bottom
mutex_t bottom_mutex;		//Mutex to guard only one thread can modify the thread_stack_bottom
mutex_t join_mutex;			//Mutex to guard to check whether one thread can be joined
unsigned int stack_size = 0;//The multithread thread stack
int main_thread_id = 0;		//The main thread id

/* Free stack elements for newly created threads. 
 * When an thread exits, the stack is appended 
 * to this list for reuse.
 */
tqueue_t free_stacks;

/* A list of active threads */
tqueue_t threads;

/**
 * @brief Counting the tailing zeros
 *
 * @param number The specified number
 *
 * @return The number of tailing zeros
 *
 */
int ctz(unsigned int number){
	
	int result = 0;
	while(number !=0 ){
		if(number&1){
			break;
		}else{
			result++;
			number = number>>1;
		}
	}

	return result;
}

/** @brief Helper funtion to init thread_t structure
 *
 *  @param thread The pointer to the thread_t object
 *  @param tid The thread id
 */
void init_thread_t(thread_t *thread,int tid){
	sem_init(&thread->sem,0);
	thread->join_count = 0;
	/* By default we set main_thread_id as the joiner_tid.
	 * It will recycle the thread resource.
	 */
	thread->joiner_tid = main_thread_id;
	thread->stack = NULL;
	thread->exit_value = 0;
	thread->exited = 0;
	thread->tid = tid;	
}


/**
 *	@brief helper funtion used by autostack to check
 *	whether our program is in an multithread environment
 *
 *	@return Return 1 for multithread environment,
 *	0 otherwise
 */
int is_multithread(){
	return stack_size!=0;
}

/**
 * @brief Get the new stack address
 *
 * @param base Current stack bottom
 * @param size The allocated stack size
 *
 * @return The new stack bottom
 */
void *get_new_bottom(void *bottom,int size){
	/* First we need to align current base to PAGE_SIZE,
	 * then align the allocated stack to PAGE_SIZE
	 */
	return PAGE_ALLIGNED_ADDR(bottom)-GET_STACK_SIZE(size);
}

/**
 * @brief Start the execution of the new thread.
 * It is implemented with assembly.
 *
 * @param stack The thread's stack top
 * @param func	The thread's execution function
 * @param arg	The argument for the thread function
 *
 * @return Void
 */
int thr_start(void *stack,void *(*func)(void *), void *arg);

/** 
 *  @brief Helper function to check whether one specified
 *  thread exists or not
 *
 *  @param thread Pointer to thread_t object
 *  @param tid The thread id
 *
 *  @return Return 1 if the two tids are equal, 
 *  0 otherwise
 *
 */
int filter_by_tid(void *thread,void *tid){
	return ((thread_t*)(thread))->tid==(int)tid;
}

/**
 * @brief Helper function to check whether one specified
 * thread exists or not.
 *
 * @param thread Pointer to thread_t object
 * @param tid The thread id
 *
 * @return Return 0 if the two tids are equal,
 * 1 otherwise
 *
 */
int thread_not_exit(void *thread,void* tid){
	return ((thread_t *)thread)->tid != (int)tid;
}

/**
 *  @brief Check whether one thread is alive or not
 *
 *  @param tid The specified thread id
 *
 *  @return 1 if the thread is alive, 0 otherwise
 *
 */
int is_alive(int tid){
    thread_t *t = t_queue_find(&threads,filter_by_tid,(void*)tid);
    return t!=NULL;
}

/**
 * @brief Try to add one thread to the working thread list.
 * If the specified thread id exists, the tid is added to 
 * the list; otherwise, it can not be added. The operation
 * is atomic
 *
 * @param tid The thread id to be added
 * 
 * @return Return 1 if the tid is successfully added, 
 * otherwise 0;
 *
 */
int test_and_add_thread(int tid){
	int added;
    thread_t *thr = (thread_t *)malloc(sizeof(thread_t));
    init_thread_t(thr,tid);

	/* Use thread safe queue tqueue to ensure the atomicity */
	added = t_queue_test_and_enq(&threads,thread_not_exit,(void*)tid,thr);
	
	/* If we can not add, delete the allocated memory */
	if(!added){
		free(thr);
	}

	return added;
}

/** 
 * @brief Get one free stack from the free_stacks list,
 * if the list is empty, allocate a new memory space with
 * new_pages() function
 *
 * @return The allocated free stack 
 */
stack_el_t *get_free_stack(){
	void *new_bottom;
	int new_page_result;
	stack_el_t *result = (stack_el_t*)t_dequeue(&free_stacks);

	/* The stack list is empty*/
	if(result == NULL){
		/* Use mutex to ensure on one else is modifying */
		mutex_lock(&bottom_mutex);
		new_bottom = get_new_bottom(thread_stack_bottom,stack_size);
		new_page_result = new_pages(new_bottom,GET_STACK_SIZE(stack_size));

		/* Error occured when allocating new pages*/
		if(new_page_result!=0){
            result = NULL;
        }else{
            result = (stack_el_t *)malloc(sizeof(stack_el_t));
            /* Can not allocate any memory */
            if(result == NULL){
                /* Release the newly allocated memory */
                remove_pages(new_bottom);
            }else{
                /* Update the stack element field */
                result->bottom = new_bottom;

                /* Update the stack bottom */
                thread_stack_bottom = new_bottom;
            }
        }
		/* Release the lock */
		mutex_unlock(&bottom_mutex);
	}

	return result;
}


/** 
 * @brief Add a free stack space to stack lists
 *
 * @stack The stack element to be added
 * @return Void
 */
void add_free_stack(stack_el_t *stack){
	if(stack!=NULL){
		t_enqueue(&free_stacks,stack);
	}
}

/**
 * @brief The thread library initlization function.
 * The function should be executed before any thread
 * related function to be called. Also before the 
 * malloc function to be called.
 *
 * @param size The stack size for the newly created
 * thread. If the size is 0, we will return error.
 *
 * @return Return 0 on success, negtive otherwise
 */
int thr_init(unsigned int size){
	if(size == 0){
		return E_INVALID_ARG;
	}

	stack_size = size;
	/* The starting stack bottom is derived from
	 * autostack.
	 */
	thread_stack_bottom  = stack_bottom;

	/* Initialize two thread safe queues */
	t_queue_init(&threads);
	t_queue_init(&free_stacks);

	/* Initialize two mutex */
	mutex_init(&join_mutex);
	mutex_init(&bottom_mutex);

	/* We store the main thread's tid, it is used to 
	 * improve the performace of thr_getid()
	 */
	main_thread_id = gettid();
	test_and_add_thread(main_thread_id);

	return 0;
}

/**
 * @brief
 *
 */
int thr_join(int tid, void **statusp){
	int result = 0;
	int can_join = 0;

	thread_t *thread = t_queue_find(
			&threads,filter_by_tid,(void*)tid);

	/* Can not find the specified thread id, return*/
	if(thread == NULL){
		return -1;
	}

	/* Use mutex to protect critical section. Check
	 * whether the destination thread can be joined
	 */
	mutex_lock(&join_mutex);
	can_join = (thread->join_count==0);

	/* Wheter this thread can be joined or not, after
	 * our attempt to join, the join count should be 1
	 */
	thread->join_count = 1;
	mutex_unlock(&join_mutex);

	if(can_join){
		/* We can join, wait till the joined thread to
		 * wake up. If the the thread exists before we
		 * call wait, the semaphore's value is 1, we
		 * can execute without waiting. 
		 */
		sem_wait(&thread->sem);
		/* The joined thread has exited, we can remove
		 * it from the thread list
		 */
		thread = t_queue_find_and_deq(&threads,filter_by_tid,(void*)tid);
    	/* Update the exit value */
		if(statusp!=NULL){
			*statusp = (void*)thread->exit_value;
    	}
	}else{
		/* We can not join */
		result = -1;
	}
	return result;
}

/**
 * @brief recycle all thread resource
 *
 * @return Void
 *
 */
void recycle_resource(){
	stack_el_t *stack;
	stack = t_dequeue(&free_stacks);

	while(stack != NULL){
		remove_pages(stack->bottom);
		stack = t_dequeue(&free_stacks);
	}
}


/**
 * @brief Exit the thread. On thread exit, it
 * will return the allocated stack to the
 * free stack list for re-use. Other resource
 * will be recycled by the join thread.
 * 
 * If the exiting thread is the main thread,
 * it will recycle all the resources allocated
 * by the thread lib, such as pages from new_pages()
 *
 * @param status The exit status
 *
 * @return Void
 */
void thr_exit(void *status){
	int tid;
	stack_el_t *stack;

	tid = thr_getid();
	thread_t *thread = t_queue_find(
			&threads,filter_by_tid,(void*)tid);
	/* Set exit status, it is used by the joining thread */
	thread->exit_value = (int)status;

	stack = thread->stack;
	/* Mark itself as dead */
	thread->exited = 1;

	/* Wake up the joining thread */
	sem_signal(&thread->sem);
	
	/* We return free stack to system for reuse */
	add_free_stack(stack);

	/* If the main thread exit, we need to recycle
	 * all the related resource.
	 */
	if(tid == main_thread_id){
		//recycle_resource();
	}

	exit(0);
}

/**
 * @brief Get current thread stack top,
 * it is a helper function to retrieve
 * current thread id
 *
 * @return The address of current stack
 * top
 */
void *thread_stack_top(){
	int zeros = ctz(PAGE_SIZE);
	int stack_top = (((int)get_ebp())&((-1)<<zeros))+PAGE_SIZE;
	return (void*)stack_top;
}

/**
 *	@brief Get current thread id
 *	When creating a new thread, we stored the tid
 *	in the first 4 bytes of its stack top.
 *
 *	@return Current thread id
 */
int thr_getid(){
	return gettid();
}

/**
 * @brief Defers execution of invoking thread to a later
 * time in favor of the thread with ID tid.
 *
 * We only add an wrapper on the system call yield()
 *
 * @param tid The thread id to be yielded.
 *
 */
int thr_yield(int tid){
	return yield(tid);
}

/**
 * @brief The routine the new thread runs. It is called
 * by the assembly function thr_start after the thread_fork
 * system call.
 *
 * @param func The routine executed in the new thread
 * @param arg  The argument for routine func
 *
 * @return Void
 *
 */
void thr_exec_fun(void *(*func)(void *),void *arg){
	void *result;
	/* We try to add the newly created thread both
	 * in the child thread and in the parent thread.
	 * As the interleaving between parent and child
	 * is not determined, we need make sure both parent
	 * and child can see the new tid in the global
	 * thread list.
	 */
	test_and_add_thread(thr_getid());

	/* Execute the thread routine */
	result = func(arg);

	/* In case the thread routine has not called thr_exit() */
	thr_exit(result);
}

int thr_create(void *(*func)(void *), void *arg ){
	int tid;
	void *stack_top;

	/* Allocate a free stack for the new thread */
	stack_el_t *stack = get_free_stack();

	/* If no more memory is available, return */
	if(stack==NULL){
		return -1;
	}

	/* new_pages() returns the low address, we need caculate the
	 * top address of the thread.
	 */
	stack_top = (stack->bottom)+GET_STACK_SIZE(stack_size);
	*(int*)stack_top = 0;


	tid = thr_start(stack_top,func,arg);
	/* Add new thread id in the parent part, in case the child
	 * thread has started, however the tid has not been added.
	 * Otherwise, the thr_join() function may fail
	 */
	test_and_add_thread(tid);

	return tid;
}

