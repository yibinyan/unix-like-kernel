/**
 * @file cond.c
 *
 * @brief This file contains functions related to Conditional Varaiable
 *
 * @author Yibin Yan
 */
#include <cond_type.h>
#include <syscall.h>
#include <stdlib.h>
#include <simics.h>

extern int is_alive(int tid);

/**
 * @brief Initialize Conditional Variable
 *
 * @param cv The struct of Contional Variable
 *
 * @return void
 */
int cond_init(cond_t *cv){
   
    list_init(&(cv->wait_list));
     
    /* initialize mutex */
    if (mutex_init(&(cv->cond_mutex)) < 0){
        return -1;  /* Fail to initialize mutex, return -1*/
    }else{
        /* success */
        return 0;
    }
}

/**
 * @brief Destroy Conditional Variable
 *
 * @param cv The struct of Contional Variable
 *
 * @return void
 */
void cond_destroy(cond_t *cv){
    if (is_empty(&cv->wait_list)){
        mutex_destroy(&(cv->cond_mutex));    
        list_destroy(&(cv->wait_list));
    }else{
        /* illegal to destory a condition variable while 
         * threads are blocked waiting on it */
        exit(-1);
    }
}

/**
 * @brief Allow thread to wait for a condition and relaese associated mutex
 *
 * @param cv Struct of Contional Variable
 * @param mp Associated mutex that thread need s to hold to check the condition
 *
 * @return void
 */
void cond_wait(cond_t *cv, mutex_t *mp){
   
    mutex_lock(&(cv->cond_mutex));
    node_t *waitlist_node = (node_t *)malloc(sizeof(node_t));
    waitlist_node->value = (void *)gettid();
    append(&(cv->wait_list), waitlist_node);
    mutex_unlock(mp);
    
    /* NEED TO RUN ATOMICALLY */
    mutex_unlock(&(cv->cond_mutex));
    /* pause current thread */
    int reject = 0;
    deschedule(&reject);
    
    mutex_lock(mp);
}

/**
 * @brief Wake up a threads waiting on the condition variable
 *
 * @param cv Struct of Contional Variable
 *
 * @return void
 */
void cond_signal(cond_t *cv){
    int tid;
    node_t *node;

    mutex_lock(&(cv->cond_mutex));
    
    if ((node = pop(&(cv->wait_list))) != NULL){
        tid = (int)node->value;
        
        if (is_alive(tid) && (make_runnable(tid) < 0)){
            /* thread is still on schedule, append node and signal again*/
            append(&(cv->wait_list), node);
            mutex_unlock(&(cv->cond_mutex)); 
            yield(-1);
            cond_signal(cv);
            return;
        }else{
            free(node);
        }
    }
    
    mutex_unlock(&(cv->cond_mutex)); 
}

/**
 * @brief Wake up all threads waiting on the condition variable
 *
 * @param cv Struct of Contional Variable
 *
  @return void
 */
void cond_broadcast(cond_t *cv){
    int tid;
    node_t *node;

    mutex_lock(&(cv->cond_mutex));
    
    while ((node = pop(&(cv->wait_list))) != NULL){
        /* signal all threads in the wait queue  */
        tid = (int)node->value;
        free(node);
        make_runnable(tid);
    }

    mutex_unlock(&(cv->cond_mutex));
} 
