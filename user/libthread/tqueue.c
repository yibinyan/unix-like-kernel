/**
 * @file tqueue.c
 *
 * @brief A general purpose thread safe queue.
 *
 */
#include <tqueue.h>
#include <stdlib.h>
#include <simics.h>
#include <thread.h>

/**
 *	@brief Init the thread safe queue
 *
 *  @param queue The pointer to queue
 */
void t_queue_init(tqueue_t *queue){
    mutex_init(&queue->lock);
    queue_init(&queue->queue);
}

/**
 * @brief Get the element from queue head.
 *
 * @param queue The pointer to queue
 *
 * @return The head of the queue, or NUll
 * if the queue is empty
 *
 */
void *t_dequeue(tqueue_t *queue){
    void *data = NULL;
    mutex_lock(&queue->lock);
    data = dequeue(&queue->queue);
    mutex_unlock(&queue->lock);

    return data;
}

/**
 * @brief Add an element to the tail of the queue
 *
 * @param queue The pointer to the queue
 * @param data  The enqueue data
 *
 * @return Void
 *
 */
void t_enqueue(tqueue_t *queue, void *data){
    mutex_lock(&queue->lock);
    enqueue(&queue->queue,data);
    mutex_unlock(&queue->lock);
}


/**
 * @brief Check whether the queue is empty or not
 *
 * @param queue The pointer to the queue
 *
 * @return 1 if the queue is empty, otherwise 0
 */
int  t_is_queue_empty(tqueue_t *queue){
    int result = 0;
    mutex_lock(&queue->lock);
    result = is_queue_empty(&queue->queue);
    mutex_unlock(&queue->lock);

    return result;
}

/**
 * @brief Destroy the queue
 *
 * @param queue The pointer to the queue
 *
 * @return Void
 */
void t_queue_destory(tqueue_t *queue){
    mutex_lock(&queue->lock);
    queue_destory(&queue->queue);
    mutex_unlock(&queue->lock);
}

/**
 *	@brief Destory the queue along with the data it contains
 *
 *	@param queue The pointer to the queue
 *	@param release The release function for the containing data
 *
 *	@return Void
 *
 */
void t_queue_destory_with_data(tqueue_t *queue, void(*release)(void*)){
    mutex_lock(&queue->lock);
    queue_destory_with_data(&queue->queue,release);
    mutex_unlock(&queue->lock);
}

/**
 * @brief Find element in the queue which meets the filter function.
 * It will return the first one which meets the filter.
 *
 * @param queue The pointer to the queue
 * @param filter The function pointer specifies the criterion
 * @param arg The argument used by the filter
 *
 * @return The found element. NULL if no element found.
 *
 */
void *t_queue_find(tqueue_t *queue,int(*filter)(void*,void*),void*arg){
	void *result = NULL;
	mutex_lock(&queue->lock);
	queue_el_t *ele = queue->queue.head;
	while(ele!=NULL){
		if(filter(ele->value,arg)){
			result = ele->value;
			break;
		}
		ele = ele->next;
	}
	mutex_unlock(&queue->lock);
	return result;
}

/**
 * @brief An atomic operation to find and delete one element in the queue 
 *
 */
void *t_queue_find_and_deq(tqueue_t *queue,int(*filter)(void*,void*),void*arg){
	void *result = NULL;
	queue_el_t *prev, *cur;
	
	mutex_lock(&queue->lock);
	cur = queue->queue.head;
	prev = cur;
	
	while(cur!=NULL){
		if(filter(cur->value,arg)){
			break;
		}
		prev = cur;
		cur = cur->next;
	}

	 /* If there is a match */
    if(cur!=NULL){
		result = cur->value;
        /* The removed ele is at the head */
        if(cur==queue->queue.head){
            /* There is only one element in the queue */
            if(cur->next == NULL){
                queue->queue.head = NULL;
                queue->queue.tail = NULL;
            }else{
                queue->queue.head = cur->next;
            }
        }else{
            prev->next = cur->next;
        }

        /* If the delete element is the tail, update
         * the new tail
         */
        if(cur==queue->queue.tail){
            queue->queue.tail = prev;
            queue->queue.tail->next = NULL;
        }

        /* free the node memory */
        free(cur);
    }

	mutex_unlock(&queue->lock);
	return result;
}


/**
 * @brief An atomic operation to check whether one criterion meets 
 * and add one element in the queue 
 *
 */
int t_queue_test_and_enq(tqueue_t *queue,int(*filter)(void*,void*),void* arg,void* data){
	int meet = 1;
	mutex_lock(&queue->lock);
	queue_el_t *ele = queue->queue.head;
	int i = 0;
	while(ele!=NULL){
		if(!filter(ele->value,arg)){
			meet = 0;
			break;
		}
		ele = ele->next;
	}

	if(meet){
		enqueue(&queue->queue,data);
	}

	i = 0;
	ele = queue->queue.head;
	while(ele!=NULL){
		ele = ele->next;
	}
	mutex_unlock(&queue->lock);

	return meet;
}
