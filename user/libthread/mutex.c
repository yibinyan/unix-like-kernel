#include <mutex.h>
#include <syscall.h>
#include <thread.h>
#include <stdlib.h>


/**
 *	@brief Use intel pause instruction to
 *	reduce spin_lock cost
 *
 *	@return Void
 */
extern void pause();

/**
 *	@brief We use xchg instruction to implement automic
 *	read write operation
 *
 *	@param memory The memory address to be written
 *	@param val The value to be exchanged with the memory
 *
 *	@return The value in memory
 */
int xchg(int* memory,int val);

/**
 *	@brief spin lock initialization
 *
 *	@param spin Pointer to the spin lock
 *	to be initialized
 *
 *	@return Void
 */
void spin_init(spin_t *spin){
	xchg(&spin->lock,1);
}

/**
 * @brief If the spin lock is not occupied by
 * others, obtain this spin lock
 * 
 * @param spin The pointer to the spin lock
 *
 * @return Void
 */
void spin_acquire(spin_t *spin){
	int val = xchg(&spin->lock,0);

	while(!val){
		pause();
		val = xchg(&spin->lock,0);
	}
}

/**
 * @brief Release an occupied lock
 *
 * @param spin The pointer to the spin lock
 *
 * @return Void
 */
void spin_release(spin_t *spin){
	xchg(&spin->lock,1);
}

/**
 * @brief Init the mutex, it is initialized as
 * available, with an empty waiting queue
 *
 * @param mp The mutex_t pointer
 *
 */
int mutex_init( mutex_t *mp ){
	spin_init(&mp->lock);
	mp->free = 1;
	mp->destroyed = 0;
	queue_init(&mp->waiting);
	
	return 0;
}

/*void mutex_destroy(mutex_t *mp){
	exit(-1);
}

void mutex_unlock( mutex_t *mp){
	spin_release(&mp->lock);
}

void mutex_lock(mutex_t *mp){
	spin_acquire(&mp->lock);
}*/

/**
 * @brief Release a mutex. If there are threads in
 * the waiting queue, pick one and wake it up
 * 
 * @param mp The mutex_t pointer
 */
void mutex_unlock( mutex_t *mp ){
	/* First acquire the spin lock */
	spin_acquire(&mp->lock);

	/* take one from the waiting queue */
	void *value = dequeue(&mp->waiting);

	//No waiting list
	if(value==NULL){
		mp->free = 1;
	}else{
		/* We have not implemented the atomic operation,
		 * however, we check the make_runnable status code,
		 * if the thread has not descheduled, we call 
		 * unlock recursively.
		 */
		int status = make_runnable((int)value);
		if(status<0){
			/* Enqueue the thread id again */
			enqueue(&mp->waiting,value);
			/* Release the spin lock to avoid deadlock */
			spin_release(&mp->lock);
			yield(-1);
			mutex_unlock(mp);
		}
	}

	spin_release(&mp->lock);
}

/**
 * @brief Destroy a mutex. If a mutex is hold by
 * a lock, exit the program.
 *
 * @param mp The mutex pointer
 *
 */
void mutex_destroy(mutex_t *mp){
	spin_acquire(&mp->lock);
	if(mp->free){
		mp->destroyed = 1;
		queue_destory(&mp->waiting);
	}else{
		spin_release(&mp->lock);
		exit(-1);
	}
	spin_release(&mp->lock);
}

/**
 * @brief Lock the mutex. If the mutex is hold by
 * others, add self to the waiting queue and deschedule.
 * Otherwise hold the mutex, and set its status to busy
 *
 * @param mp The mutex pointer
 *
 */
void mutex_lock(mutex_t *mp){
	spin_acquire(&mp->lock);

	/* Already destoryed, exit */
	if(mp->destroyed){
		spin_release(&mp->lock);
		exit(-1);
	}

	if(!mp->free){
		/* The mutex is busy, add to the waiting queue*/
		int tid = thr_getid();
		enqueue(&mp->waiting,(void*)tid);
		spin_release(&mp->lock);
		int wait = 0;
		deschedule(&wait);
	}else{

		/* Mark the mutex as busy */
		mp->free = 0;
		spin_release(&mp->lock);
	}
}

