/** @file rwlock_type.h
 *  @brief This file defines the type for reader/writer locks.
 */

#ifndef _RWLOCK_TYPE_H
#define _RWLOCK_TYPE_H

#include <cond.h>
#include <mutex.h>

typedef struct rwlock_test {
    int running_reader;  /* numbers of reader that are running  */
    int waiting_reader;  /* numbers of reader that are waiting */
    int running_writer;  /* numbers of writer that are running */
    int waiting_writer;  /* numbers of writer that are waiting */
    
    cond_t r_cond;
    cond_t w_cond;  
    
    /* using mutex to make sure each time can only one of 
    * threads doing jobs that related to same reader_writer_lock */ 
    mutex_t rw_mutex;   
    
} rwlock_t;

#endif /* _RWLOCK_TYPE_H */
