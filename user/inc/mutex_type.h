/** @file mutex_type.h
 *  @brief This file defines the type for mutexes.
 */

#ifndef _MUTEX_TYPE_H
#define _MUTEX_TYPE_H

#include <queue.h>

typedef struct spin {
	int lock;
} spin_t;

typedef struct mutex {
	spin_t lock;
	int free;
	int destroyed;
	queue_t waiting;
} mutex_t;

void spin_init(spin_t *spin);
void spin_acquire(spin_t *spin);
void spin_release(spin_t *spin);

#endif /* _MUTEX_TYPE_H */
