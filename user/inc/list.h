#ifndef __LIST_H_
#define __LIST_H_

#include <mutex.h>
/** 
 * @brief A general list node
 */
typedef struct node{
    /** @brief pointers to the next element in the list */
    struct node *next;
    /** @brief the low address of the allocated stack */
    void *value;
}node_t;

/**
 * @brief Thread-safe queue implemented with linked list 
 * with two pointers, one to header and the other to tail.
 * 
 * We add new element to the tail, and get free element from
 * the head
 */
typedef struct{
    /** @brief pointers to the head of the linked list */
    node_t *head;
    /** @brief pointers to the tail of the linked list */
    node_t *tail;
    /** @brief mutex used to protect multi-threaded access */
    mutex_t lock;
    /** @brief current stack base */
    void *value;
}list_t;

node_t *pop(list_t *list);
node_t *find_element(list_t *list,int(*filter)(node_t* node,void* arg),void* arg);
void append(list_t *list, node_t *node);
int is_empty(list_t *list);
void remove_element(list_t *list,node_t *node);

void list_init(list_t *list);
void list_destroy(list_t *list);

#endif
