/** @file cond_type.h
 *  @brief This file defines the type for condition variables.
 */

#ifndef _COND_TYPE_H
#define _COND_TYPE_H

#include <list.h>

typedef struct cond {
    list_t wait_list;
    mutex_t cond_mutex;    /* mutex used by cond */
} cond_t;

#endif /* _COND_TYPE_H */
