#include <mutex.h>
#include <queue.h>

typedef struct{
	queue_t queue;
	mutex_t lock;
}tqueue_t;

void t_queue_init(tqueue_t *queue);
void *t_dequeue(tqueue_t *queue);
void t_enqueue(tqueue_t *queue, void *data);
int  t_is_queue_empty(tqueue_t *queue);
void t_queue_destory(tqueue_t *queue);
void t_queue_destory_with_data(tqueue_t *queue, void(*release)(void*));
void *t_queue_find(tqueue_t *queue,int(*filter)(void*,void*),void*);
void *t_queue_find_and_deq(tqueue_t *queue,int(*filter)(void*,void*),void*);
int t_queue_test_and_enq(tqueue_t *queue,int(*filter)(void*,void*),void*,void*);
