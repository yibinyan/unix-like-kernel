/** @file sem_type.h
 *  @brief This file defines the type for semaphores.
 */

#ifndef _SEM_TYPE_H
#define _SEM_TYPE_H

#include <list.h>

typedef struct sem {
    int count;
    list_t wait_list;
    mutex_t sem_mutex;  /* mutex that used for manipulate sem exclusively */ 
} sem_t;

#endif /* _SEM_TYPE_H */
