#ifndef __SPIN_LOCK_H_
#define __SPIN_LOCK_H_

typedef struct spin {
    int lock;
} spin_t;

typedef struct mutex {
	spin_t lock;
} mutex_t;

void spin_init(spin_t *spin);
void spin_acquire(spin_t *spin);
void spin_release(spin_t *spin);

#endif
