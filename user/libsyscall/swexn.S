#include <syscall_int.h>

.global swexn
swexn:
	push %ebp
	mov  %esp,%ebp
	push %esi
	lea  8(%ebp),%esi
	INT  $SWEXN_INT
	pop %esi
	leave
	ret
