/**
 * @file pagefault_handler.c
 *
 * @brief handle page fault that related to non present page
 *
 * @author Yibin Yan
 *
 **/

#include <simics.h>
#include <syscall.h>
#include <stdlib.h>
#include <test.h>
#include <pagefault_handler.h>

extern void *stack_bottom;
void *exception_stack_base;

void pagefault_handler(void *arg, ureg_t *ureg){
    int is_nonpresent_page;  /* equal 1 if is due to non-present page  */    
      

 
    /* check whether exception is caused by pagefault */
    if (ureg->cause == SWEXN_CAUSE_PAGEFAULT){
        /* if exception is due to non-present page, then the last bit of error_code is 0 */ 
        is_nonpresent_page = !(ureg->error_code & 0x1);
        
        if (is_nonpresent_page){
            
            if (!is_multithread()){
                /* n specify how many page should alloc */
                int n = ((unsigned int)stack_bottom - ureg->cr2) / PAGE_SIZE;
                if ((((unsigned int)stack_bottom - ureg->cr2) % PAGE_SIZE) > 0){
                    n = n + 1;
                }    
            
                /* update stack_buttom */
                stack_bottom = (void *)((unsigned int)stack_bottom - PAGE_SIZE * n);
                
                if (new_pages(stack_bottom, PAGE_SIZE * n) != 0){
                    /* fail to allocate page */
                    exit(-1);
                }
            }
        
            swexn(exception_stack_base, pagefault_handler, 0, ureg);
        }
    }
}
