/**
 * @file autostack.c
 *
 * @brief install autostack
 *
 * @author Yibin Yan
 */

#include <simics.h>
#include <syscall.h>
#include <pagefault_handler.h>
#include <stdlib.h>

void *stack_bottom;     /* point to stack buttom  */
void *stack_top;     /* point to stack buttom  */
void *exception_stack_base;  /* point to the base of exception stack  */

/**
 * @brief Install autostack
 *
 * @param stack_high The highest address of initial stack
 * @param stack_low  The lowest address of initial stack
 *
 * @return void
 */
void install_autostack(void *stack_high, void *stack_low)
{ 
    stack_bottom = stack_low;
    stack_top = stack_high;

    void *exception_stack_addr = (void *)_malloc(64);     /* alloc 64 bytes for exception stack*/
    exception_stack_base = (void *)((unsigned int)exception_stack_addr + 64);    

    swexn(exception_stack_base, pagefault_handler, 0, 0);
}



