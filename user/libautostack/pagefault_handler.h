#ifndef _PAGEFAULT_HANDLER_H_
#define _PAGEFAULT_HANDLER_H_

extern int is_multithread();
void pagefault_handler(void *, ureg_t *);

#endif
