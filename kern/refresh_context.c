#include <refresh_context.h>
#include <process.h>
#include <simics.h>
#include <set_context.h>
#include <set_seg.h>

void refresh_context(){

    schedule();
       
    if (current->context.eip >= 16 * 1024 * 1024){
        set_context_on_usermode(&current->context);
    }else{
        set_context(&current->context);
    }
}
