#include <timer_defines.h>
#include "handler.h"
#include <process.h>
#include "keyboard.h"
#include "timer.h"
#include "syscall_handler.h"
#include "exception_handler.h"

uint32_t num_ticks = 0; /* total number of timer interrupts */
void (*callback_func)(unsigned int);    /*callback function*/
uint32_t idt_base_addr;

/** @brief Tell PIC the interrupt has been processed
 *    
 *  @return Void.
 */
void int_ack();

/** @brief Keyboard handler
 *    
 *  @return Void.
 */
void de_handler();
int sys_gettid_handler();
void sys_halt_handler();
//void sys_gettid_handler();

/*function to install handler*/
int handler_install(){
    
    idt_base_addr = (uint32_t)idt_base();
   
    keyboard_setup();
    timer_setup();
    syscall_handler_setup();   
    exception_handler_setup(); 
  
    /* configure IDT*/
    lidt((void *)idt_base_addr, LIMIT);
    return 0;
}

void int_ack(){
    /* tell PIC the interrupt has been processed */
    outb(INT_CTL_PORT, INT_ACK_CURRENT);
}

void de_handler(){
}

