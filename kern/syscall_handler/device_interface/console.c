/** @file console.c 
 *
 *  @brief The implementation of the console driver.
 *
 *  @author Bixian Bao (bixianb)
 *  @bug No known bugs.
 */

#include <x86/asm.h>
#include <stdlib.h>
#include <simics.h>
#include <console.h>

/************************************************************
 *		    Some CONSTANTS used by the console driver       *
 ************************************************************/
#define HIDDEN_CURSOR_ROW 128	/* row of the hidden cursor */
#define HIDDEN_CURSOR_COL 128	/* col of the hidden cursor */
#define MAX_OFFSET ((CONSOLE_WIDTH)*(CONSOLE_HEIGHT)-1)

#define NEWLINE '\n'			/* special character for '\n'*/
#define CARRIAGE_RETURN '\r'	/* special character for '\r'*/
#define BACKSPACE '\b'			/* special character for '\b'*/
#define BLANK ' '				/* special character for ' ' */


/************************************************************
 *               Some  Helper functions                     *
 ************************************************************/

/*	The offset is used by graphics adaptor to d*/

#define GET_LSB(offset)	((offset)&0xFF)					/* Get LSB from offset */
/* Get MSB from offset */
#define GET_MSB(offset) (((offset)&0xFF00)>>8)
/* Get offset from lsb and msb */
#define GET_OFFSET(lsb,msb) ((lsb)+((msb)<<8))

#define CONTENT(r,c) (*(char*)(CONSOLE_MEM_BASE+2*(CONSOLE_WIDTH*(r)+(c)))) 
#define COLOR(r,c)	(*(char*)(CONSOLE_MEM_BASE+2*(CONSOLE_WIDTH*(r)+(c))+1))

/************************************************************
 *                   Global variables                       *
 ************************************************************/
/**
 * The current input postion is specified by the row and col
 * variable. If cursor is enabled, they should equal to the
 * the cursor position.
 */
static int cursor_row = 0;
static int cursor_col = 0;

/**
 * Variable indicating whether current cursor is hidden or not
 */
static int is_cursor_hidden = 0;

/**
 * Variable storing current color scheme, default value is 
 * FGND_WHITE|BGND_BLACK. When no color is specified when
 * drawing a character, this value is used.
 */
static int term_color = FGND_WHITE|BGND_BLACK;

/**	@brief Get position row from offset
 *
 * 	Helper function converting position row from offset
 *
 *	@param offset The offset of the position
 *	@return The converted position row
 */
static int
get_row_from_offset(int offset)
{
	return offset/CONSOLE_WIDTH;
}

/**	@brief Get position col from offset
 *
 *  Helper function converting position col from offset
 *
 *  @param offset The offset of the position
 *  @return The converted position col
 */
static int 
get_col_from_offset(int offset)
{
	return offset%CONSOLE_WIDTH;
}

/** @brief Check whether a position specified by (row, col)
 *  is valid or not
 *
 *  @param row The position row
 *  @param col The position col
 *  @return Return 1 if the position is valid, 0 otherwise
 *
 */
static int 
is_valid_position(int row,int col)
{
	return (row>=0&&row<CONSOLE_HEIGHT)
		&&(col>=0&&col<CONSOLE_WIDTH);
}

/** @brief Check whether a given char is valid or not
 *
 *  @param ascii The ascii value of the character
 *  @return Return 1 if the character is valid, 0 otherwise
 *
 */
static int 
is_valid_char(int ascii)
{
	return ascii>=0 && ascii<=127;
}

/** @brief Check whether a given color is valid or not
 *
 *  Color should be no less than 0 and be no greater
 *  than 0xFF
 *
 *  @param color The color value being checked
 *  @return Return 1 if the color is valid, 0 otherwise
 *
 */
static int 
is_valid_color(int color)
{
	return ((color>=0)&&(color<=0xFF));
}

/** @brief Caculate the offset from row and col
 *
 * 	@param row The position row
 * 	@param col The position col
 * 	@return The converted offset from row and col
 */
static int 
get_offset(int row,int col)
{
	/* Check whether argument valid */
	if(!is_valid_position(row,col)){
		return 0;
	}

	return row*CONSOLE_WIDTH + col;
}

/** @brief Write to the specified position on the screen without
 *  update the current position row and col
 *
 *  Internal used function. The process of drawing a character on
 *  screen includes two steps: the first step is to draw a character
 *  if it is printable; the second step is to update the current
 *  drawing postion. This function carries out the first step.
 *
 *  @param row The row of the position to be written
 *  @param col The col of the position to be written
 *  @param ch  The written character
 *  @return Void
 *
 */
static void 
_putchar(int row,int col,char ch)
{
	/* Check whether argument valid */
	if(!is_valid_position(row,col)){
		return;
	}

	CONTENT(row,col) = ch;
	COLOR(row,col) = term_color;
}

/** @brief Scroll up the screen for specified lines
 *	
 *	When scroll up lines, we use the cell values below n lines to 
 *	replace current cell value. And the last n lines of screen is
 *	filled with blank character. In order to keep the code simple,
 *	we use the fact that the return value of get_char(row,col) for
 *	an invalid position is a BLANK character
 *
 *  @param lines The number of lines to be scrolled
 *  @return Void
 */
static void 
scroll_up(int n)
{
	char ch;
	int row,col;
	
	/* Check whether argument valid*/
	if(n <= 0){
		return;
	}

	for(row = 0;row<CONSOLE_HEIGHT; row++){
		for(col = 0;col<CONSOLE_WIDTH; col++){
			/* We use the fact the the return value of
			 * get_char(row,col) is BLANK if the position
			 * exceeds the CONSOLE_HEIGHT to keep code
			 * simple. Do not worry about the invalid
			 * memory access.
			 */
			ch = get_char(n+row,col);
			_putchar(row,col,ch);
		}
	}
}

/** @brief Write the non-printable new line character. It only
 *  updates current position. If no room left, it also scoll
 *  up one line.
 *
 *  @return Void
 */
static void 
write_new_line(){
	/* If there is room for the new line, move the 
	 * postion to the start of the next line.
	 */
	if(cursor_row < CONSOLE_HEIGHT-1){
		set_cursor(++cursor_row,0);
	}else{
		/* Set the new position to the start of the
		 * last line
		*/
		set_cursor(CONSOLE_HEIGHT-1,0);
		
		/* scroll up one line*/
		scroll_up(1);
	}
}

/** @brief Write the non-printable carriage return character.
 * 	It only updates current position.
 *
 *	@return Void
 */
static void
write_carriage_return(){
	set_cursor(cursor_row,0);
}

/**	@brief Write backspace character. It moves one position back,
 *  and clear that character. If the cursor is at position (0,0),
 *  it does nothing.
 *
 *  @return Void
 */
static void
write_backspace(){
	/* Cursor at the beginning */
	if(cursor_col == 0 && cursor_row == 0){
		return;
	}

	/* The new offset equals to current offset-1 */
	int offset = get_offset(cursor_row, cursor_col) - 1;

	
	int new_row = get_row_from_offset(offset);
	int new_col = get_col_from_offset(offset);

	/* Clear the previous character */
	_putchar(new_row,new_col,BLANK);
	set_cursor(new_row,new_col);
}

/** @brief Write a printable character on the screen. Before writing,
 *  we will check whether current position exceeds the MAX_OFFSET
 *
 *	@return Void
 */
static void 
write_printable_char(char ch){
	int offset = get_offset(cursor_row,cursor_col);
	
	/* If current writing position exceeds the MAX_OFFSET, scroll
	 * up one line, and move the position to the beginning of the
	 * last line
	 */
	if(offset>MAX_OFFSET){
		scroll_up(1);
		cursor_row = CONSOLE_HEIGHT-1;
		cursor_col = 0;
	}

	/* Write the character to the position */
	_putchar(cursor_row,cursor_col,ch);

	/* New writing offset should be current offset plus one */
	offset = get_offset(cursor_row,cursor_col)+1;
	

	/* Calculate the new row and col */
	set_cursor(get_row_from_offset(offset),
			get_col_from_offset(offset));
}

/** @brief Prints character ch at the current location
 *         of the cursor.
 *
 *  If the character is a newline ('\n'), the cursor is
 *  be moved to the beginning of the next line (scrolling if necessary).
 *  If the character is a carriage return ('\r'), the cursor
 *  is immediately reset to the beginning of the current
 *  line, causing any future output to overwrite any existing
 *  output on the line.  If backspace ('\b') is encountered,
 *  the previous character is erased.  See the main console.c description
 *  for more backspace behavior.
 *
 *  @param ch the character to print
 *  @return The input character
 */
int putbyte( char ch )
{
	switch(ch){
		case NEWLINE:
			write_new_line();
			break;
		case CARRIAGE_RETURN:
			write_carriage_return();
			break;
		case BACKSPACE:
			write_backspace();
			break;
		default:
			write_printable_char(ch);
	}
  
	return ch; 
}

/** @brief Prints the string s, starting at the current
 *         location of the cursor.
 *
 *  If the string is longer than the current line, the
 *  string fills up the current line and then
 *  continues on the next line. If the string exceeds
 *  available space on the entire console, the screen
 *  scrolls up one line, and then the string
 *  continues on the new line.  If '\n', '\r', and '\b' are
 *  encountered within the string, they are handled
 *  as per putbyte. If len is not a positive integer or s
 *  is null, the function has no effect.
 *
 *  @param s The string to be printed.
 *  @param len The length of the string s.
 *  @return Void.
 */
int putbytes( const char *s, int len )
{
	/* Check whether argument is valid or not */
	if(s == NULL||len <= 0){
		return -1;
	}

	int index = 0;
	while(s[index]!='\0'&&index<=len-1){
		putbyte(s[index]);
        index++;
	}
    
    return 0;
}

/** @brief Changes the foreground and background color
 *         of future characters printed on the console.
 *
 *  If the color code is invalid, the function has no effect.
 *
 *  @param color The new color code.
 *  @return 0 on success or integer error code less than 0 if
 *          color code is invalid.
 */
int _set_term_color( int color )
{
	if(!is_valid_color(color)){
		return -1;
	}
	
	term_color = color;

	return 0;
}

/** @brief Writes the current foreground and background
 *         color of characters printed on the console
 *         into the argument color.
 *  @param color The address to which the current color
 *         information will be written.
 *  @return Void.
 */
void get_term_color( int *color )
{
	if(color == NULL){
		return;
	}

	*color = term_color;
}

/** @brief Sets the position of the cursor to the
 *         position (row, col).
 *
 *  Subsequent calls to putbytes should cause the console
 *  output to begin at the new position. If the cursor is
 *  currently hidden, a call to set_cursor() does not show
 *  the cursor.
 *
 *  @param row The new row for the cursor.
 *  @param col The new column for the cursor.
 *  @return 0 on success or integer error code less than 0 if
 *          cursor location is invalid.
 */
int set_cursor( int row, int col )
{
	int offset;

	/* Check whether argument is valid*/
	if(row<0 || col<0){
		return -1;
	}

	/* Calculate the cursor offset */
	offset = row * CONSOLE_WIDTH + col;
	
	if(!is_cursor_hidden){
		/* Write to the low byte */
		outb(CRTC_IDX_REG,CRTC_CURSOR_LSB_IDX);
		outb(CRTC_DATA_REG,GET_LSB(offset));
		
		/* Write to the high byte */
		outb(CRTC_IDX_REG,CRTC_CURSOR_MSB_IDX);
		outb(CRTC_DATA_REG,GET_MSB(offset));
	}

	/* For the writing position, we only set it to
	 * valid positions.
	 */
	if((row<CONSOLE_HEIGHT)&&(col<CONSOLE_WIDTH)){
		cursor_row = row;
		cursor_col = col;
	    return 0;
	}else{
        return -1;
    }
}

/** @brief Writes the current position of the cursor
 *         into the arguments row and col.
 *  @param row The address to which the current cursor
 *         row will be written.
 *  @param col The address to which the current cursor
 *  @return 0 on success or integer error code less than 0 if
 *          either argument is invalid.*.
 */
int get_cursor( int *row, int *col )
{
	if(row == NULL || col==NULL){
		return -1;
	}

	*row = cursor_row;
	*col = cursor_col;

    return 0;
}

/** @brief Hides the cursor.
 *
 *  Subsequent calls to putbytes do not cause the
 *  cursor to show again.
 *
 *  @return Void.
 */
void hide_cursor()
{
	set_cursor(HIDDEN_CURSOR_ROW,HIDDEN_CURSOR_COL);
	is_cursor_hidden = 1;
}

/** @brief Shows the cursor.
 *  
 *  If the cursor is already shown, the function has no effect.
 *
 *  @return Void.
 */
void show_cursor()
{
	is_cursor_hidden = 0;
	set_cursor(cursor_row,cursor_col);
}

/** @brief Clears the entire console.
 *
 * The cursor is reset to the first row and column
 *
 *  @return Void.
 */
void clear_console()
{
	/* For each screen element, write blank character ' ' */
	int row,col;
	for(row=0;row<CONSOLE_HEIGHT;row++){
		for(col = 0;col<CONSOLE_WIDTH;col++){
			_putchar(row,col,BLANK);
		}
	}

	/* reset cursor position*/
	set_cursor(0,0);
}

/** @brief Prints character ch with the specified color
 *         at position (row, col).
 *
 *  If any argument is invalid, the function has no effect.
 *
 *  @param row The row in which to display the character.
 *  @param col The column in which to display the character.
 *  @param ch The character to display.
 *  @param color The color to use to display the character.
 *  @return Void.
 */
void draw_char( int row, int col, int ch, int color )
{
	if(!is_valid_position(row,col)
			||!is_valid_char(ch)
			||!is_valid_color(color)){
		return;
	}

	CONTENT(row,col) = ch;
	COLOR(row,col) = color;
}

/** @brief Returns the character displayed at position (row, col).
 *  @param row Row of the character.
 *  @param col Column of the character.
 *  @return The character at (row, col).
 */
char get_char( int row, int col )
{
	/* Check whether argument valid */
	if(!is_valid_position(row,col)){
		return BLANK;
	}
	
	return CONTENT(row,col);
}

