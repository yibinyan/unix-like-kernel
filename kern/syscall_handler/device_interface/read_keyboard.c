#include <keyboard.h>
#include <console.h>
#include <stdlib.h>
#include <simics.h>

#define READLINE_BUFFER_SIZE 2048

/* we set a special buffer which just for readline, 
 * getchar won't read from this buffer */
int readline_buffer[READLINE_BUFFER_SIZE];
int rl_buf_start = 0; /* begin index of available buffer */
int rl_buf_size = 0; /* current size of readline buffer */
int rl_buf_current_index = 0; /* current index */

static spin_t read_lock = {1};
static spin_t readline_lock = {1};
spin_t deschedule_lock = {0};

int readchar(void)
{
    uint8_t scode; /* scan code */
    int ch = -1;
    kh_type argu_ch;  /* argumented character return by process_scancode() */
    int read_count = 0; /*count numbers of scode we have read*/
    int next = 0;   /* next position of buffer where is available to read */

    deschedule_lock.lock = 0;

    while (current_buf_size == 0){
        /* keyboard buffer does not currently contain a valid character */
        int flag = 0;
       
        enqueue(&waiting_queue, (void *)sys_gettid_handler());
        sys_deschedule(&flag);
        //spin_acquire(&deschedule_lock);
    }
    

    spin_acquire(&read_lock); 
    scode = (uint32_t)dequeue(&key_buffer);
    argu_ch = process_scancode(scode);
   
   /* find first valid character */ 
    while (!KH_HASDATA(argu_ch) || KH_ISMAKE(argu_ch)){
        
        read_count++;
        
        if (read_count < current_buf_size){
            scode = (uint32_t)dequeue(&key_buffer);
            argu_ch = process_scancode(scode); 
        }else{
            break;
        } 
    }

    if (read_count < current_buf_size){
        next = read_count + 1;

        ch = KH_GETCHAR(argu_ch);        
    
    }else{
        current_buf_size = 0;
        spin_release(&read_lock);
        return -1;
    }

    current_buf_size = current_buf_size - next; /* update current size */

    spin_release(&read_lock);

    return ch;
}

int _readline(int len, char *buf){
    int max_length = 2048;
    int ch;
    int read_num; /* numbers of characters read */ 
    int i;    

    if ((len < 0) || (len > max_length) || (buf == NULL)){
        return -1;
    }

    
    for (read_num = 0; read_num < len; read_num++){
        //lprintf("IN READLINE!"); 
        ch = readchar();
       
        if (ch < 0){
            read_num--;
            continue;
        }

        /* if ch is backspace, then move cursor back and remove 1 chracter */
        if (ch == '\b'){
            /*if not any input, no effect*/
            if (read_num > 0){    
                putbyte(ch);    
                read_num--;
                spin_acquire(&readline_lock);

                if (rl_buf_current_index == 0){
                    rl_buf_current_index = READLINE_BUFFER_SIZE - 1;
                }else{
                    rl_buf_current_index--;
                }

                spin_release(&readline_lock);    
            }
    
            read_num--;
        }else{
            putbyte(ch);
            
            spin_acquire(&readline_lock);
            readline_buffer[rl_buf_current_index] = ch;
            rl_buf_current_index = (rl_buf_current_index + 1)
                                        % READLINE_BUFFER_SIZE;
            rl_buf_size++;    
            spin_release(&readline_lock);    
        }

        if (ch == '\n'){
            read_num++;
            break;
        }
    }

    spin_acquire(&readline_lock);

    for (i = 0; i < len; i++){
        
        if (i < rl_buf_size){
            int temp
                 = readline_buffer[(i + rl_buf_start) % READLINE_BUFFER_SIZE];
            
            if (temp == '\n'){
                i++;
                break;
            }else{
                buf[i] = temp;
            }
        }else{
            break;
        }
    }
    
    rl_buf_start = (i + rl_buf_start) % READLINE_BUFFER_SIZE;
    rl_buf_size = rl_buf_size - i;

    spin_release(&readline_lock); 
    return i;
}
