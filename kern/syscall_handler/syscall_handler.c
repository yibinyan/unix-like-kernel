#include "handler.h"
#include "syscall_handler.h"
#include "exception_handler.h"

void syscall_handler_setup(){
    struct gate sys_gettid_gate = {0};
    struct gate sys_fork_gate = {0};
    struct gate sys_halt_gate = {0};
	struct gate sys_exec_gate = {0};
	struct gate sys_wait_gate = {0};
	struct gate sys_set_status_gate = {0};
	struct gate sys_vanish_gate = {0};
	struct gate sys_deschedule_gate = {0};
	struct gate sys_make_runnable_gate = {0};
	struct gate sys_yield_gate = {0};
	struct gate sys_new_pages_gate = {0};
	struct gate sys_remove_pages_gate = {0};
    struct gate sys_set_term_color_gate = {0};
    struct gate sys_set_cursor_pos_gate = {0};
    struct gate sys_get_cursor_pos_gate = {0};
    struct gate sys_print_gate = {0};
    struct gate sys_readline_gate = {0};
    struct gate sys_readfile_gate = {0};
	struct gate sys_get_ticks_gate = {0};
	struct gate sys_sleep_gate = {0};
	struct gate sys_thr_fork_gate = {0};
    struct gate swexn_gate = {0};
    
    set_user_interrupt_gate(&sys_fork_gate, sys_fork_wrapper);    
    set_user_trap_gate(&sys_gettid_gate, sys_gettid_wrapper);
    set_user_trap_gate(&sys_halt_gate, sys_halt_wrapper);
	set_user_trap_gate(&sys_exec_gate, sys_exec_wrapper);
    set_user_trap_gate(&sys_wait_gate, sys_wait_wrapper);
    set_user_trap_gate(&sys_set_status_gate, sys_set_status_wrapper);
    set_user_trap_gate(&sys_vanish_gate, sys_vanish_wrapper);
	set_user_trap_gate(&sys_deschedule_gate, sys_deschedule_wrapper);
	set_user_trap_gate(&sys_make_runnable_gate, sys_make_runnable_wrapper);
	set_user_trap_gate(&sys_yield_gate, sys_yield_wrapper);
	set_user_interrupt_gate(&sys_new_pages_gate, sys_new_pages_wrapper);
	set_user_interrupt_gate(&sys_remove_pages_gate, sys_remove_pages_wrapper);
    set_user_trap_gate(&sys_set_term_color_gate, sys_set_term_color_wrapper);
    set_user_trap_gate(&sys_set_cursor_pos_gate, sys_set_cursor_pos_wrapper);
    set_user_trap_gate(&sys_get_cursor_pos_gate, sys_get_cursor_pos_wrapper);
    set_user_trap_gate(&sys_print_gate, sys_print_wrapper);
    set_user_trap_gate(&sys_readline_gate, sys_readline_wrapper);
    set_user_trap_gate(&sys_readfile_gate, sys_readfile_wrapper);
	set_user_trap_gate(&sys_get_ticks_gate, sys_get_ticks_wrapper);
	set_user_trap_gate(&sys_sleep_gate, sys_sleep_wrapper);
	set_user_interrupt_gate(&sys_thr_fork_gate, sys_thr_fork_wrapper);
    set_user_trap_gate(&swexn_gate, sys_swexn_wrapper);

    INSTALL_GATE(SET_TERM_COLOR_INT, sys_set_term_color_gate);
    INSTALL_GATE(SET_CURSOR_POS_INT, sys_set_cursor_pos_gate);
    INSTALL_GATE(GET_CURSOR_POS_INT, sys_get_cursor_pos_gate);
    INSTALL_GATE(PRINT_INT, sys_print_gate);
    INSTALL_GATE(GETTID_INT, sys_gettid_gate);
    INSTALL_GATE(FORK_INT, sys_fork_gate);
    INSTALL_GATE(HALT_INT, sys_halt_gate);
    INSTALL_GATE(EXEC_INT, sys_exec_gate);
    INSTALL_GATE(WAIT_INT, sys_wait_gate);
    INSTALL_GATE(SET_STATUS_INT, sys_set_status_gate);
    INSTALL_GATE(VANISH_INT, sys_vanish_gate);
    INSTALL_GATE(DESCHEDULE_INT, sys_deschedule_gate);
    INSTALL_GATE(MAKE_RUNNABLE_INT, sys_make_runnable_gate);
    INSTALL_GATE(YIELD_INT, sys_yield_gate);
	INSTALL_GATE(NEW_PAGES_INT, sys_new_pages_gate);
	INSTALL_GATE(REMOVE_PAGES_INT, sys_remove_pages_gate);
    INSTALL_GATE(READLINE_INT, sys_readline_gate);
    INSTALL_GATE(READFILE_INT, sys_readfile_gate);
	INSTALL_GATE(GET_TICKS_INT,sys_get_ticks_gate);
	INSTALL_GATE(SLEEP_INT,sys_sleep_gate);
	INSTALL_GATE(THREAD_FORK_INT,sys_thr_fork_gate);
    INSTALL_GATE(SWEXN_INT, swexn_gate);
}


