#include "syscall_handler.h"
#include "send_instruction.h"

void sys_halt_handler(){
    if (sim_in_simics()){
        sim_halt();
    }else {
        send_halt_instruction();    
    }
}
