#include "handler.h"
#include "syscall_handler.h"

extern int sys_make_runnable(int tid);

int sys_make_runnable_handler(int tid){
    return sys_make_runnable(tid);

}

