#include "handler.h"
#include "vm.h"
#include "syscall_handler.h"

extern int exec(char *path,char **argv);

int sys_exec_handler(char *name,char** argv){
    unsigned int *current_ebp = get_ebp();
    unsigned int *buttom_of_pusha = current_ebp + 4;

	int i = 0;
	while(argv[i]!=0){
		i++;
	}

    current->context.edi = *(buttom_of_pusha);
    current->context.esi = *(buttom_of_pusha + 1);
    current->context.ebp = *(buttom_of_pusha + 2);
    current->context.ebx = *(buttom_of_pusha + 4);
    current->context.edx = *(buttom_of_pusha + 5);
    current->context.ecx = *(buttom_of_pusha + 6);
    current->context.eip = *(buttom_of_pusha + 8);
    current->context.esp = *(buttom_of_pusha + 11);

	switch_to_kvm();
    int result = exec(name,argv);

    *(buttom_of_pusha) = current->context.edi;
    *(buttom_of_pusha + 1) = current->context.esi;
    *(buttom_of_pusha + 2) = current->context.ebp;
    *(buttom_of_pusha + 4) = current->context.ebx;
    *(buttom_of_pusha + 5) = current->context.edx;
    *(buttom_of_pusha + 6) = current->context.ecx;
    *(buttom_of_pusha + 7) = current->context.eax;
    *(buttom_of_pusha + 8) = current->context.eip;
    *(buttom_of_pusha + 11) = current->context.esp;

	switch_to_uvm();

    return result;
}

