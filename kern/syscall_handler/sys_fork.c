#include "handler.h"
#include "syscall_handler.h"
#include <schedule_and_switch.h>
#include "vm.h"

int sys_fork_handler(){
    unsigned int *current_ebp = get_ebp();
    unsigned int *buttom_of_pusha = current_ebp + 2;
            
    current->context.edi = *(buttom_of_pusha);
    current->context.esi = *(buttom_of_pusha + 1);
    current->context.ebp = *(buttom_of_pusha + 2);
    current->context.ebx = *(buttom_of_pusha + 4);
    current->context.edx = *(buttom_of_pusha + 5);
    current->context.ecx = *(buttom_of_pusha + 6);
    current->context.eip = *(buttom_of_pusha + 8);
    current->context.esp = *(buttom_of_pusha + 11);
    
	switch_to_kvm();
    process_t *child = fork_process(current);
    /* set parent process's return value to child pid */ 
    current->context.eax = child->pid;
    /* set child process's return value to 0 */ 
    child->context.eax = 0;
	switch_to_uvm();
    
    return child->pid;
}
