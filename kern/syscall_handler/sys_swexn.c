#include "handler.h"
#include "syscall_handler.h"
#include "exception_handler.h"
#include "swexn_list.h"
#include "eflags.h"
#include <stdlib.h>

int is_ureg_valid(ureg_t *ureg){
  
    unsigned int iopl = ((ureg->eflags) & (0x3 << EFL_IOPL_SHIFT));
 
    if (iopl != EFL_IOPL_RING0){
        return 0;
    }
    
    return 1;
}

int sys_swexn_handler(void *esp3, void *eip, void *arg, ureg_t *newureg){    
    int valid_address = 16 * 1024 * 1024;
    int result;
    
    if ((esp3 == NULL) || (eip == NULL)){
        /*if esp3 and/or rip are zero, de-register*/
        delete_from_swexn_list(sys_gettid_handler());
        result = 0;
    } else if (((unsigned int)esp3 < valid_address) 
                || ((unsigned int)eip < valid_address)){
        /* eip and/or esp is clearly wrong*/
        result = -1;
    } else {
        /* cause == -1 means _swexn is called by system call swexn */
        result =  _swexn(-1, esp3, eip, arg, NULL);
    }

    if ((result == 0) && (newureg != NULL)){
        if (!is_ureg_valid(newureg)){
            /* newureg not valid  */
            return -1;
        }

        /* only if everything goes right, newureg work */
        unsigned int *current_ebp = get_ebp();
        unsigned int *buttom_of_pusha = current_ebp + 6;

        *(buttom_of_pusha) = newureg->edi;
        *(buttom_of_pusha + 1) = newureg->esi;
        *(buttom_of_pusha + 2) = newureg->ebp;
        *(buttom_of_pusha + 4) = newureg->ebx;
        *(buttom_of_pusha + 5) = newureg->edx;
        *(buttom_of_pusha + 6) = newureg->ecx;
        *(buttom_of_pusha + 7) = newureg->eax;
        *(buttom_of_pusha + 8) = newureg->eip;
        *(buttom_of_pusha + 9) = newureg->cs;
        *(buttom_of_pusha + 11) = newureg->esp;
        *(buttom_of_pusha + 12) = newureg->ss;
    }

    return result;
}

