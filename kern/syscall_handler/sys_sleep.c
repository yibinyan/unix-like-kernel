#include "handler.h"
#include "syscall_handler.h"
#include "process.h"

int sys_sleep_handler(int ticks){

	if(ticks<0){
		return -1;
	}else if(ticks == 0){
		return 0;
	}

    sys_sleep(ticks);
	return 0;
}

