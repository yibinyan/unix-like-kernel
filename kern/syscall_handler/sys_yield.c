#include "handler.h"
#include "syscall_handler.h"
#include "simics.h"

extern int sys_yield(int tid);

int sys_yield_handler(int tid){

    int result = sys_yield(tid);
	return result;

} 
