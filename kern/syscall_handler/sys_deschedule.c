#include "handler.h"
#include "syscall_handler.h"

extern int sys_deschedule(int* status);

int sys_deschedule_handler(int *status){
    return sys_deschedule(status);
}

