#include "handler.h"
#include "syscall_handler.h"

extern void sys_set_status(int);

void sys_set_status_handler(int status){
	 sys_set_status(status);
}
