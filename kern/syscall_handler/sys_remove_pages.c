#include "handler.h"
#include "syscall_handler.h"
#include "vm.h"


int sys_remove_pages_handler(void *base){
	 switch_to_kvm();
	 int result = sys_remove_pages(base);
	 switch_to_uvm();
	 return result;
}

