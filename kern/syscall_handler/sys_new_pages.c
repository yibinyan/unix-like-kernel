#include "handler.h"
#include "syscall_handler.h"
#include "vm.h"

int sys_new_pages_handler(void *base,int len){
	switch_to_kvm();
	int result = sys_new_pages(base,len);
	switch_to_uvm();
	return result;
}

