/**
 * The 15-410 kernel project.
 * @name loader.c
 *
 * Functions for the loading
 * of user programs from binary 
 * files should be written in
 * this file. The function 
 * elf_load_helper() is provided
 * for your use.
 */
/*@{*/

/* --- Includes --- */
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <exec2obj.h>
#include <loader.h>
#include <elf_410.h>
#include <simics.h>
#include "asms.h"
#include <x86/cr.h>
#include "malloc_internal.h"
#include "pagetable.h"
#include "process.h"
#include "vm.h"



/**
 * Copies data from a file into a buffer.
 *
 * @param filename   the name of the file to copy data from
 * @param offset     the location in the file to begin copying from
 * @param size       the number of bytes to be copied
 * @param buf        the buffer to copy the data into
 *
 * @return returns the number of bytes copied on succes; -1 on failure
 */
int getbytes( const char *filename, int offset, int size, char *buf )
{
  int found=0;
  int index=0;

  /* Find object by name */
  for(;index<exec2obj_userapp_count;index++){
	  if(!strcmp(exec2obj_userapp_TOC[index].execname,filename)){
		  found = 1;
		  break;
	  }
  }

  if(!found){
	  return -1;
  }
  
  exec2obj_userapp_TOC_entry entry = exec2obj_userapp_TOC[index];
  /* If offset exceeds file length, return  */
  if(offset>entry.execlen){
	  return 0;
  }

  index = offset;
  int length = 0;
  while(length<size&&(offset+length)<entry.execlen){
	  char copy = entry.execbytes[offset+length];
	  buf[length]=copy;
	  length++;
  }

  return length;
}

/**
 * @brief Check whether elf exists or not.
 *
 * @param elf_name The specified elf name.
 *
 * @return 1 if the specified elf exists, 0 otherwise.
 *
 */
int elf_exists(const char* elf_name){
	return !elf_check_header(elf_name);
}


/**
 * @brief Load program data to process. Map virtual memory to physical
 * memory for the process, and copy the data to the different sections
 *
 * @param process The process pointer which the elf file to be loaded
 *
 * @param elf_name The elf file name
 *
 * @return The entry point of the elf file
 *
 */
int load_elf(pagetable_t *ptable,const char* elf_name){
	int entry = 0;
	simple_elf_t * elf = (simple_elf_t*)_malloc(sizeof(simple_elf_t));

	elf_load_helper(elf,elf_name);

	/* Map virtual address */
    map_virtual_address(ptable,
			(void*)elf->e_txtstart,elf->e_txtlen);
    map_virtual_address(ptable,
			(void*)elf->e_datstart,elf->e_datlen);
    map_virtual_address(ptable,
			(void*)elf->e_rodatstart,elf->e_rodatlen+PAGE_SIZE);
    map_virtual_address(ptable,
			(void*)elf->e_bssstart,elf->e_bsslen);
	
	/* Backup for old page directory */
	int old_cr3 = get_cr3();

	/* Switching to new address space and load image */
	set_cr3((int)(ptable->page_directory));

	/* Load program data to specified address */
	getbytes(elf_name,elf->e_txtoff,
			elf->e_txtlen,(char*)elf->e_txtstart);

	getbytes(elf_name,elf->e_datoff,
			elf->e_datlen,(char*)elf->e_datstart);

	getbytes(elf_name,elf->e_rodatoff,
			elf->e_rodatlen,(char*)elf->e_rodatstart);

	entry = elf->e_entry;

	/* Free dynamic memory */
	_free(elf);

	/* restore cr3 */
	set_cr3(old_cr3);

	/* After load image to memory, we should change the pagetable privilege*/
	set_virtual_address_mask(ptable,
			(void*)elf->e_txtstart,elf->e_txtlen,USER_RO_MASK);
	set_virtual_address_mask(ptable,
			(void*)elf->e_rodatstart,elf->e_rodatlen,USER_RO_MASK);

	return entry;
}

/*@}*/
