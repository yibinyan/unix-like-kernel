#ifndef _SET_CONTEXT_H_
#define _SET_CONTEXT_H_

#include <process.h>

void set_context(ctx_t *);
void set_context_on_usermode(ctx_t *);

#endif
