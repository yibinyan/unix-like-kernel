#ifndef __PAGETABLE_H_
#define __PAGETABLE_H_

//#include "process.h"

//#define LOW_MASK 0xE7F
#define LOW_MASK 0x007
#define KERNEL_MASK 0x003
#define USER_RW_MASK 0x007
#define USER_RO_MASK 0x005

#ifndef PAGE_SIZE
#define PAGE_SIZE (1<<12)
#endif

#define PTE_COUNT (1<<10)


typedef struct pagetable{
	int **page_directory;
} pagetable_t;

void init_pagetable(pagetable_t *ptable);
void destroy_pagetable(pagetable_t *ptable);
int  unmap_page(pagetable_t *ptable,void *va);
void map_page(pagetable_t *ptable, void *vf, void *pf);
void map_page_with_mask(pagetable_t *ptable, void *vf, void *pf,int mask);
void set_page_mask(pagetable_t *ptable,void *va,int mask);
int is_mapped_va(pagetable_t *ptable,void *va);
void release_allocated_user_space_pf(pagetable_t *ptable);
void clone_pagetable(pagetable_t *dest,pagetable_t *src);


#endif
