#ifndef _GATES_H_
#define _GATES_H_

#include <asm.h>
#include <seg.h>

#define OTHER_BITS_TRAP_KERNEL 0x8F00 /* other bits of gate: DPL=00,P=1,D=1 */
#define OTHER_BITS_TRAP_USER 0xEF00 /* other bits of gate: DPL=11,P=1,D=1 */
#define OTHER_BITS_INTERRUPT_KERNEL 0x8E00 /* other bits of gate: DPL=00,P=1,D=1 */
#define OTHER_BITS_INTERRUPT_USER 0xEE00 /* other bits of gate: DPL=11,P=1,D=1 */
#define INSTALL_GATE(x,y) ((*(struct gate *)(idt_base_addr + (x) * 8)) = (y)) 

/* interrupt gate */
struct gate{
    uint32_t last_32bits;
    uint32_t first_32bits;
};

void set_user_trap_gate(struct gate *target, void *wrapper);
void set_kernel_trap_gate(struct gate *target, void *wrapper);
void set_user_interrupt_gate(struct gate *target, void *wrapper);
void set_kernel_interrupt_gate(struct gate *target, void *wrapper);

#endif

