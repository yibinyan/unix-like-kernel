#ifndef _SYSCALL_HANDLER_
#define _SYSCALL_HANDLER

#include "handler.h"

int sys_fork_handler();
int sys_gettid_handler();
void sys_halt_handler();

void sys_vanish_handler();
int sys_exec_handler(char *name,char** argv);
int sys_wait_handler(int *status);
int sys_deschedule_handler(int *status);
int sys_make_runnable_handler(int status);
int sys_yield_handler(int tid);
void sys_set_status_handler(int status);
int sys_new_pages_handler(void *base,int len);
int sys_remove_pages_handler(void *base);
unsigned int sys_get_ticks_handler();

void syscall_handler_setup();
int sys_sleep_handler(int ticks);
int thr_sys_fork_handler();

#endif 
