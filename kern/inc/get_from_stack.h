#ifndef __GET_FROM_STACK_H
#define __GET_FROM_STACK_H

unsigned int *get_ebp();
unsigned int *get_ret_addr(unsigned int *);
unsigned int *set_eax(unsigned int);
void move_esp_and_call(unsigned int, void *, void *, void *);


#endif /* __GET_FROM_STACK_H */
