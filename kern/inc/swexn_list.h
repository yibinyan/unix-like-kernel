#ifndef _SWEXN_LIST_H_
#define _SWEXN_LIST_H_

#include <ureg.h>
#include "malloc_internal.h"

/** 
 * @brief A list node
 */
typedef struct swexn_node{
    /* @brief begin address of exception stack */
    void *esp3;
    /* @brief swexn_handler */
    void *eip;
    /* @brief argument */
    void *arg;

}swexn_node;

/**
 * A list to stored software interrupt handler of each process
 */
typedef struct swexn_list{
    /** @brief pointers to next node of  linked list */
    struct swexn_list *next;
    /** @brief tid of thread */
    int tid;
    /** @brief content of list node */
    swexn_node *content;
}list_t;


int delete_from_swexn_list(int tid);
void swexn_list_init();
void add_to_swexn_list(int tid, void *esp3, void *eip, void *arg);
int is_swexn_list_empty();
void swexn_list_destory();
swexn_node* get_from_swexn_list(int tid);
void copy_ureg_to_userspace(ureg_t *dst, ureg_t *src);

#endif
