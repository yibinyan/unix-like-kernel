/* The 15-410 kernel project
 *
 *     loader.h
 *
 * Structure definitions, #defines, and function prototypes
 * for the user process loader.
 */

#ifndef _LOADER_H
#define _LOADER_H
     
#include "pagetable.h"
#include "process.h"
/* --- Prototypes --- */

int getbytes( const char *filename, int offset, int size, char *buf );
int load_elf(pagetable_t *process,const char* elf_name);
int elf_exists(const char* elf_name);

/*
 * Declare your loader prototypes here.
 */

#endif /* _LOADER_H */
