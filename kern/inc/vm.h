#ifndef __VM_H_
#define __VM_H_

#include "pagetable.h"

#define VA_TOP ((unsigned long)0xFFFFF000)
#define STACK_TOP (1<<29)
#define K_STACK_SIZE (1<<10)
#define KERNEL_VM_PDE_NUM 4

typedef struct heap_page{
	struct heap_page *next;
	unsigned int start;
	unsigned int end;
} heap_page_t;

extern int *kpage;

void init();
void setup_memory();
void switch_to_kvm();
void switch_to_uvm();
void setup_stack(pagetable_t* ptable);
void init_kernel_vm(pagetable_t *ptable);
void copy_kernel_vm(pagetable_t * ptable);
void copy_out(pagetable_t *ptable,void* va,void* pa,int length);
void map_virtual_address(pagetable_t *pt,void *va,int length);
void map_virtual_address_with_mask(pagetable_t *pt,void *va,int length,int mask);
void set_virtual_address_mask(pagetable_t *ptable,void *va,int length,int mask);

int get_free_frame();
int get_free_frame_number();
void release_physical_frame(void *pa);

int is_overlaped_heap(heap_page_t *head,unsigned int heap_start,unsigned int heap_end);
int is_valid_heap_address(heap_page_t *head,int address);
void add_heap_address(heap_page_t *head,int start,int end);
int sys_new_pages(void * base, int len);
int sys_remove_pages(void *base);

#endif
