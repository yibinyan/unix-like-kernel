#ifndef __WRAPPER_H
#define __WRAPPER_H

void timer_wrapper();
void keyboard_wrapper();
void sys_gettid_wrapper();
void sys_fork_wrapper();
void sys_halt_wrapper();

void sys_halt_wrapper();
int sys_exec_wrapper(char *,char**);
int sys_wait_wrapper(int*);
int sys_deschedule_wrapper(int*);
int sys_make_runnable_wrapper(int);
int sys_yield_wrapper(int);
int sys_new_pages_wrapper(void *base,int len);
int sys_remove_pages_wrapper(void *base);
int sys_set_status_wrapper(int);
void sys_vanish_wrapper();

void sys_set_term_color_wrapper();
void sys_set_cursor_pos_wrapper();
void sys_get_cursor_pos_wrapper();
void sys_print_wrapper();
void sys_readline_wrapper();
void sys_readfile_wrapper();
unsigned int sys_get_ticks_wrapper();
int sys_sleep_wrapper(int);
int sys_thr_fork_wrapper();

void sys_swexn_wrapper();

#endif



