#ifndef _EXCEPTION_HANDLER_H_
#define _EXCEPTION_HANDLER_H_

#include <ureg.h>
#include "exception_wrapper.h"

void exn_divide_handler();
void exn_debug_handler();
void exn_breakpoint_handler();
void exn_overflow_handler();
void exn_boundcheck_handler();
void exn_opcode_handler();
void exn_nofpu_handler();
void exn_segfault_handler();
void exn_stackfault_handler();
void exn_protfault_handler();
void exn_pagefault_handler();
void exn_fpufault_handler();
void exn_alignfault_handler();
void exn_simdfault_handler();

int _swexn(int cause, void *esp3, void *eip, void *arg, ureg_t *newureg);

void exception_handler_setup();

/* This has already contained in syscall.h, 
 * however, we think include syscall.h in files that belong 
 * to kern is inapropriate. So we put it here */
typedef void (*swexn_handler_t)(void *arg, ureg_t *ureg);

#endif
