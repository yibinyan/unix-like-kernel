#ifndef _SET_SEG_H_
#define _SET_SEG_H_

#include <asm.h>

unsigned int user_eflags;
void run_on_usermode(uint32_t eip, uint32_t esp, uint32_t eflags);
void change_to_usermode();

#endif
