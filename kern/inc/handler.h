#ifndef _HANDLER_H_
#define _HANDLER_H_

#include <asm.h>
#include <interrupt_defines.h>
#include <seg.h>
#include "get_from_stack.h"
#include <simics.h>
#include <idt.h>
#include <syscall_int.h>
#include "process.h"
#include "wrapper.h"
#include "gates.h"

#define KEY_BUF_MAX_SIZE 1024 /* max size of key buffer */
#define LIMIT 0xFFFFFFFF /* limits of segment*/
#define CYCLE 11932 /* numer of timer cycles to generate 10ms interrupt  */

extern uint32_t idt_base_addr; 

void int_ack();

int handler_install();

#endif
