#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <asm.h>
#include <queue.h>
#include <keyhelp.h>
#include <process.h>
#include <syscall_handler.h>
#include <spinlock.h>

extern queue_t key_buffer;   /* buffer of scancode*/
extern queue_t waiting_queue;   /* buffer of scancode*/
extern uint16_t current_buf_size; /* buffer current size */

void keyboard_setup();
void keyboard_handler();
int  readchar();


#endif
