#ifndef __QUEUE_H_
#define __QUEUE_H_

/** 
 * @brief A general queue node
 */
typedef struct queue_el{
    /** @brief pointers to the next element in the list */
    struct queue_el *next;
    /** @brief the low address of the allocated stack */
    void *value;
}queue_el_t;

/**
 * @brief Thread-safe queue implemented with linked list 
 * with two pointers, one to header and the other to tail.
 * 
 * We add new element to the tail, and get free element from
 * the head
 */
typedef struct{
    /** @brief pointers to the head of the linked list */
    queue_el_t *head;
    /** @brief pointers to the tail of the linked list */
    queue_el_t *tail;
    /** @brief mutex used to protect multi-threaded access */
}queue_t;

void queue_init(queue_t *queue);
void *dequeue(queue_t *queue);
void enqueue(queue_t *queue, void* data);
int is_queue_empty(queue_t *queue);
void queue_destory(queue_t *queue);
void queue_destory_with_data(queue_t *queue, void(*release)(void*));
#endif
