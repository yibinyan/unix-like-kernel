#ifndef _EXCEPTION_WRAPPER_H_
#define _EXCEPTION_WRAPPER_H_


void exn_divide_wrapper();
void exn_debug_wrapper();
void exn_breakpoint_wrapper();
void exn_overflow_wrapper();
void exn_boundcheck_wrapper();
void exn_opcode_wrapper();
void exn_nofpu_wrapper();
void exn_segfault_wrapper();
void exn_stackfault_wrapper();
void exn_protfault_wrapper();
void exn_pagefault_wrapper();
void exn_fpufault_wrapper();
void exn_alignfault_wrapper();
void exn_simdfault_wrapper();

#endif
