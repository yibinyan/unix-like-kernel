#include <x86/seg.h>

.globl run_on_usermode
.globl get_current_cs_seg

run_on_usermode:
    cli
    
    movl $SEGSEL_USER_DS, %eax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    
    mov 4(%esp), %ecx /* eip */   
    mov 8(%esp), %eax  /* esp */ 
    mov 12(%esp), %edx  /* eflags */
   
    push $SEGSEL_USER_DS
    push %eax
    push %edx
    push $SEGSEL_USER_CS
    pushl %ecx
    
    iret

get_current_cs_seg:
    push %ebp
    mov  %esp, %ebp
    mov  %cs, %eax
    leave
    ret

