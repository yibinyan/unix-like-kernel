/** @file kernel.c
 *  @brief An initial kernel.c
 *
 *  You should initialize things in kernel_main(),
 *  and then run stuff.
 *
 *  @author Harry Q. Bovik (hqbovik)
 *  @author Fred Hacker (fhacker)
 *  @bug No known bugs.
 */

#include <common_kern.h>

/* libc includes. */
#include <stdio.h>
#include <simics.h>                 /* lprintf() */

/* multiboot header file */
#include <multiboot.h>              /* boot_info */

/* x86 specific includes */
#include <x86/asm.h>                /* enable_interrupts() */
#include <x86/cr.h>
#include "handler.h"
#include "loader.h"
#include "vm.h"
#include "pagetable.h"
#include "set_seg.h"
#include "eflags.h"
#include "seg.h"
#include "asms.h"
#include "process.h"
#include <malloc_internal.h>
#include <console.h>

/** @brief Kernel entrypoint.
 *  
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(mbinfo_t *mbinfo, int argc, char **argv, char **envp)
{
    /*
     * When kernel_main() begins, interrupts are DISABLED.
     * You should delete this comment, and enable them --
     * when you are ready.
     */
    
    handler_install();
    clear_console();
 
    //lprintf("test!");
    setup_memory();
    setup_processes();
    set_cr0(get_cr0()|CR0_PG);
//lprintf("CR0:  %x", (unsigned int)get_cr0());
    set_cr4(get_cr4()|CR4_PGE);
//  set_user_mode_seg(); 
//  uint32_t current_cs = get_current_cs_seg();


/*    
    process_t *shell_process = alloc_pcb();
    int shell_entry = load_elf(&shell_process->ptable, "shell");
    (shell_process->context).eip = shell_entry;
    shell_process->status = RUNNABLE;
*/

    process_t *idle_process = alloc_pcb();
    int idle_entry = load_elf(&idle_process->ptable, "idle");
    (idle_process->context).eip = idle_entry;
    idle_process->status = RUNNABLE;

    init_process = alloc_pcb();
    int init_entry = load_elf(&init_process->ptable, "init");
    (init_process->context).eip = init_entry;
    init_process->status = RUNNABLE;
    
    schedule();

   //unsigned int kernel_stack = (unsigned int)_malloc(4*PAGE_SIZE);
    //kernel_stack = kernel_stack + 4 * PAGE_SIZE;
    //set_esp0(kernel_stack);

    uint32_t user_program_eip = current->context.eip;
    uint32_t user_program_esp = current->context.esp;
    
    user_eflags = get_eflags();

    /* Arrange bit 1 and alignment checking to be on */
    user_eflags = (user_eflags | EFL_RESV1) & ~EFL_AC;
    /* turn on IF */
    user_eflags = user_eflags | EFL_IF;
    /* set IOPL to Ring 0 */
    user_eflags = (user_eflags & ~(0x3 << EFL_IOPL_SHIFT)) | EFL_IOPL_RING0;

    run_on_usermode(user_program_eip, user_program_esp, user_eflags);

    lprintf("Hello from a brand new kernel!");
    lprintf("test!");

    while (1) {
        continue;
    }

    return 0;
}
