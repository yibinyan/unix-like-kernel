#include "process.h"
#include <keyhelp.h>
#include "handler.h"
#include "keyboard.h"
#include "syscall_handler.h"

#include "schedule_and_switch.h"

extern spin_t deschedule_lock;
queue_t key_buffer;   /* buffer of scancode*/
queue_t waiting_queue;

uint16_t current_buf_size = 0; /* buffer current size */

void keyboard_setup(){
    struct gate keyboard_gate = {0};
    
    /* set keyboard gate */
    set_kernel_trap_gate(&keyboard_gate, keyboard_wrapper);
    /* install gate */
    INSTALL_GATE(KEY_IDT_ENTRY, keyboard_gate);

    queue_init(&key_buffer);
    queue_init(&waiting_queue);
}


/* put scancode into buffer */
void keyboard_handler(){
    //disable_interrupts();

    if (current_buf_size < KEY_BUF_MAX_SIZE){
        // buffer is available
        uint8_t scode = inb(KEYBOARD_PORT);
        enqueue(&key_buffer, (void *)((int)scode));
        current_buf_size++;
        
        if ((current_buf_size > 0) && (!is_queue_empty(&waiting_queue))){
            int tid = (int)dequeue(&waiting_queue);
            sys_make_runnable(tid);
        }
    }

    int_ack();
}

