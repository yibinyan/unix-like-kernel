#include "handler.h"
#include "timer.h"
#include <timer_defines.h>
#include <seg.h>
#include <eflags.h>
#include <set_seg.h>
#include <schedule_and_switch.h>
#include "process.h"
#include "vm.h"

unsigned int ticks;

void init_timer(){

	ticks = 0;
	
    outb(TIMER_MODE_IO_PORT, TIMER_SQUARE_WAVE);
    /* first send least significant byte*/
    outb(TIMER_PERIOD_IO_PORT, CYCLE & 0xFF);
    /* then most significant byte */
    outb(TIMER_PERIOD_IO_PORT, ((CYCLE >> 8) & 0xFF));
}


void timer_setup(){
    
    init_timer();
   
    struct gate timer_gate = {0};
    
    /* set timer gate */ 
    set_kernel_interrupt_gate(&timer_gate, timer_wrapper);
    /* configure IDT */
    INSTALL_GATE(TIMER_IDT_ENTRY, timer_gate);
}

void timer_handler(){
	
    ticks++;
    int_ack();
	switch_to_kvm();
	refresh_sleeping_ticks();
    schedule_and_switch(&current->context);

}


