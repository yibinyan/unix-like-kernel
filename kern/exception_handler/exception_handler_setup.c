#include "handler.h"
#include "swexn_list.h"
#include "exception_handler.h"

void exception_handler_setup(){
    struct gate exn_divide_gate = {0};
    struct gate exn_debug_gate = {0};
    struct gate exn_breakpoint_gate = {0};
	struct gate exn_overflow_gate = {0};
	struct gate exn_boundcheck_gate = {0};
	struct gate exn_opcode_gate = {0};
	struct gate exn_nofpu_gate = {0};
	struct gate exn_segfault_gate = {0};
	struct gate exn_stackfault_gate = {0};
	struct gate exn_protfault_gate = {0};
	struct gate exn_pagefault_gate = {0};
	struct gate exn_fpufault_gate = {0};
	struct gate exn_alignfault_gate = {0};
    struct gate exn_simdfault_gate = {0};
    
    set_user_trap_gate(&exn_divide_gate, exn_divide_wrapper);
    set_user_trap_gate(&exn_debug_gate, exn_debug_wrapper);
	set_user_trap_gate(&exn_breakpoint_gate, exn_breakpoint_wrapper);
    set_user_trap_gate(&exn_overflow_gate, exn_overflow_wrapper);
    set_user_trap_gate(&exn_boundcheck_gate, exn_boundcheck_wrapper);
    set_user_trap_gate(&exn_opcode_gate, exn_opcode_wrapper);
	set_user_trap_gate(&exn_nofpu_gate, exn_nofpu_wrapper);
	set_user_trap_gate(&exn_segfault_gate, exn_segfault_wrapper);
	set_user_trap_gate(&exn_stackfault_gate, exn_stackfault_wrapper);
	set_user_trap_gate(&exn_protfault_gate, exn_protfault_wrapper);
	set_user_trap_gate(&exn_pagefault_gate, exn_pagefault_wrapper);
    set_user_trap_gate(&exn_fpufault_gate, exn_fpufault_wrapper);
	set_user_trap_gate(&exn_alignfault_gate, exn_alignfault_wrapper);
    set_user_trap_gate(&exn_simdfault_gate, exn_simdfault_wrapper);    

    INSTALL_GATE(SWEXN_CAUSE_DIVIDE, exn_divide_gate);
    INSTALL_GATE(SWEXN_CAUSE_DEBUG, exn_debug_gate);
    INSTALL_GATE(SWEXN_CAUSE_BREAKPOINT, exn_breakpoint_gate);
    INSTALL_GATE(SWEXN_CAUSE_OVERFLOW, exn_overflow_gate);
    INSTALL_GATE(SWEXN_CAUSE_BOUNDCHECK, exn_boundcheck_gate);
    INSTALL_GATE(SWEXN_CAUSE_OPCODE, exn_opcode_gate);
    INSTALL_GATE(SWEXN_CAUSE_NOFPU, exn_nofpu_gate);
    INSTALL_GATE(SWEXN_CAUSE_SEGFAULT, exn_segfault_gate);
    INSTALL_GATE(SWEXN_CAUSE_STACKFAULT, exn_stackfault_gate);
    INSTALL_GATE(SWEXN_CAUSE_PROTFAULT, exn_protfault_gate);
    INSTALL_GATE(SWEXN_CAUSE_PAGEFAULT, exn_pagefault_gate);
    INSTALL_GATE(SWEXN_CAUSE_FPUFAULT, exn_fpufault_gate);
    INSTALL_GATE(SWEXN_CAUSE_ALIGNFAULT, exn_alignfault_gate);
    INSTALL_GATE(SWEXN_CAUSE_SIMDFAULT, exn_simdfault_gate);

    swexn_list_init();
}


