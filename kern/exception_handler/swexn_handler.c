#include "process.h"
#include "syscall_handler.h"
#include "exception_handler.h"
#include "swexn_list.h"
#include <stdlib.h>
#include <simics.h>
//#include <syscall.h>

int _swexn(int cause, void *esp3, 
                void *eip, void *arg, ureg_t *current_ureg){
    int tid = sys_gettid_handler();
    
    if (cause >= 0){
        /* Invoke by an exception */
        lprintf("SWEXN INVOKE BY EXCEPTION %d", cause);
        current_ureg->cause = cause; 
        swexn_node *node = get_from_swexn_list(tid);

        if (node == NULL){
            lprintf("Exception handler not found");
            /*if this thread did not register exception handler, exit*/
            sys_set_status(-1);
            sys_vanish();

        }else{
            unsigned int node_esp3 = (unsigned int)node->esp3 - 4;
            void *node_arg = node->arg;
            void *node_eip = node->eip;
            
            delete_from_swexn_list(tid);
            

            ureg_t *user_ureg = (ureg_t *)node_esp3;
            copy_ureg_to_userspace(user_ureg, current_ureg);
            
            node_esp3 = (unsigned int)(user_ureg - 1);
        
            /* find swexn_node */
            move_esp_and_call(node_esp3, node_eip, node_arg, user_ureg);
        }
    }else{
        /* invoke by system call, that means process 
         * try to register an exception  handler*/
        lprintf("Registing exception handler");
        add_to_swexn_list(tid, esp3, eip, arg);
    }      

    return 0;
}
