#include "exception_handler.h"
#include <stdlib.h>
#include <cr.h>
#include "seg.h"
#include "set_seg.h"
#include "get_from_stack.h"
#include "malloc_internal.h"
#include <simics.h>

void set_ureg(unsigned int *current_ebp, ureg_t *node){
    unsigned int *buttom_of_pusha = current_ebp + 2;
    unsigned int eip_before_interrupt = *(buttom_of_pusha + 9);
    unsigned int segment_before_interrupt = *(buttom_of_pusha + 10);   
 
    node->edi = *(buttom_of_pusha);
    node->esi = *(buttom_of_pusha + 1);
    node->ebp = *(buttom_of_pusha + 2);
    node->ebx = *(buttom_of_pusha + 4);
    node->edx = *(buttom_of_pusha + 5);
    node->ecx = *(buttom_of_pusha + 6);
    node->eax = *(buttom_of_pusha + 7);
    node->error_code = *(buttom_of_pusha + 8);
    
    node->eip = eip_before_interrupt;
    node->cs =  segment_before_interrupt;

    if (segment_before_interrupt == SEGSEL_USER_CS){
        /* mode switch happens */
        node->esp = *(buttom_of_pusha + 12);
        node->ss = SEGSEL_USER_DS;
    }else{
        /* mode switch does not happen */
        node->esp = (unsigned int)(buttom_of_pusha + 12);
        node->ss = SEGSEL_KERNEL_DS;
    }
   
    node->eflags = *(buttom_of_pusha + 11);
 
    /* store the address that caused the pagefault  */
    node->cr2 = get_cr2();
}

void exn_divide_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_DIVIDE, NULL, NULL, NULL, node);
} 

void exn_debug_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_DEBUG, NULL, NULL, NULL, node);
} 

void exn_breakpoint_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_BREAKPOINT, NULL, NULL, NULL, node);
} 

void exn_overflow_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_OVERFLOW, NULL, NULL, NULL, node);
} 

void exn_boundcheck_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_BOUNDCHECK, NULL, NULL, NULL, node);
} 

void exn_opcode_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_OPCODE, NULL, NULL, NULL, node);
} 

void exn_nofpu_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_NOFPU, NULL, NULL, NULL, node);
} 

void exn_segfault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_SEGFAULT, NULL, NULL, NULL, node);
} 

void exn_stackfault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_STACKFAULT, NULL, NULL, NULL, node);
} 

void exn_protfault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_PROTFAULT, NULL, NULL, NULL, node);
}

void exn_pagefault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_PAGEFAULT, NULL, NULL, NULL, node);
}

void exn_fpufault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_FPUFAULT, NULL, NULL, NULL, node);
}

void exn_alignfault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_ALIGNFAULT, NULL, NULL, NULL, node);
}

void exn_simdfault_handler(){
    ureg_t *node = (ureg_t *)_malloc(sizeof(ureg_t));
    unsigned int *current_ebp = get_ebp();
    set_ureg(current_ebp, node);
    _swexn(SWEXN_CAUSE_SIMDFAULT, NULL, NULL, NULL, node);
}

