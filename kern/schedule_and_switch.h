#ifndef _CONTEXT_SWITCH_
#define _CONTEXT_SWITCH__

#include <process.h>

void schedule_and_switch(ctx_t *current_context);

#endif
