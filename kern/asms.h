#ifndef __ASMS_H_
#define __ASMS_H_

/**
 *  @brief We use xchg instruction to implement automic
 *  read write operation
 *
 *  @param memory The memory address to be written
 *  @param val The value to be exchanged with the memory
 *
 *  @return The value in memory
 */
int xchg(int* memory,int val);


void jump(int start);
void set_ebp(int ebp);
void execute_on_stack(int ebp,int esp,int start);
void push(int value);
void pause();

/**
 * @brief Invalid one page of virtual memory in TLB.
 * This function is used to copy page table and
 * memory content between two processes.
 *
 * @param address The starting address of the virtual
 * memory to be invalid
 *
 * @return Void
 */
void vm_page_inval(void *address);

#endif
