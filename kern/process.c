#include <x86/cr.h>
#include "process.h"
#include "spinlock.h"
#include "pagetable.h"
#include "asms.h"
#include <stdlib.h>
#include <simics.h>
#include "vm.h"
#include <eflags.h>
#include "set_seg.h"
#include "loader.h"
#include <string.h>
#include "malloc_internal.h"
#include "schedule_and_switch.h"

/* Max Process number */
#define MAX_PROCESS 4096
/* Max argument counter */
#define MAX_ARGS 16
/* Align the address to eight bytes */
#define ALIGN(addr)	((addr) & ~3)	

/* 
 * PCBs are stored in fixed length array,
 * There are both allocated and free pcbs.
 * The thread and process share the same
 * structure.
 */
process_t processes[MAX_PROCESS];

/* The index where the next possible available
 * pcb slot index
 */
unsigned int next_process_slot = 0;

/* The global pid assigned to each new process or thread*/
unsigned int current_pid = 1;

/* Spin lock used to guard the access to the 
 * global shared process_t array
 */
static spin_t lock = {1};

/* The init process */
process_t* init_process;

/* The global current process_t pointer*/
process_t *current;

/* The index of current scheduled process. When scheduling,
 * we will start the searching index at current_scheduled+1 
 * as the next possible runnable process. We use the 
 * simplest round-robin schedule strategy.
 */
int current_scheduled = 0;

/**
 * @brief Helper funtion to init the process status
 * to be FREE
 *
 * @return Void
 */
void setup_processes(){
	int i = 0;
	for(;i<MAX_PROCESS;i++){
		processes[i].status = FREE;
	}
}

/**
 * @brief Helper function to init
 * process/thread execution context
 *
 * @param ctx Process/Thread execution
 * context to be initilized
 *
 * @return Void
 */
void init_context(ctx_t *ctx){
	ctx->eax = 0;
    ctx->ebx = 0;
    ctx->ecx = 0;
    ctx->edx = 0;
    ctx->esi = 0;
    ctx->edi = 0;
    ctx->ebp = STACK_TOP;
    ctx->esp = STACK_TOP;
    ctx->eip = 0;
}

/**
 * @brief Get the next free process_t struct
 * in the processes array. It starts at the
 * index of next_process_slot
 *
 * @return The available free process_t pointer.
 * If there is no free pcb, return NULL.
 */
process_t *find_free_pcb(){
	int search_counter = 0;
    int i = next_process_slot%MAX_PROCESS;
    
    while(search_counter<MAX_PROCESS
            &&processes[i].status!=FREE){
        search_counter++;
        i = (i+1)%MAX_PROCESS;
    }

	/* Update the next potential free pcb list */
	next_process_slot = (next_process_slot+1)%MAX_PROCESS;

	/* Have iterated the whole array, no free pcb avaliable*/
	if(search_counter==MAX_PROCESS){
		return NULL;
	}
	
	return &processes[i];
}

/**
 * @brief Allocate PCB without initlizating virtual
 * memory. It is used for thread_fork. As the new
 * thread shares the address space with parent.
 *
 * @return The newly allocated PCB
 */
process_t * alloc_pcb_without_va(){
	int pid;
	process_t *process = NULL;
    pid = current_pid++;

	spin_acquire(&lock);
	process = find_free_pcb();
	spin_release(&lock);

	if(process == NULL){
		return process;
	}

    init_context(&process->context);
    process->pid = pid;
	process->status = INIT;
    process->parent = NULL;
    process->waiting = 0;
    process->exit_code = 0;

	/* Allocate kernel stack for the process */
	process->kstack = (void*)((int)_malloc(K_STACK_SIZE)+K_STACK_SIZE-1);

	return process;
}

/**
 * @brief Allocate a new process control block
 * with new address space. It is used for fork.
 *
 * @return The newly allocated process control
 * block
 */
process_t * alloc_pcb(){
	process_t *process = alloc_pcb_without_va();

	init_pagetable(&process->ptable);
	/* Copy kernel mode shared 16 M address space */
	copy_kernel_vm(&process->ptable);
	/* Allocate process execution stack */
	setup_stack(&process->ptable);

	return process;
}


/**
 * @brief Copy the thread execution context from source
 * to destination. Helper function used in thread fork
 * and process fork
 *
 *
 * @return Void
 */
void fork_context(ctx_t* dest, ctx_t* src){
    dest->eax = src->eax;
    dest->ebx = src->ebx;
    dest->ecx = src->ecx;
    dest->edx = src->edx;
    dest->esi = src->esi;
    dest->edi = src->edi;
    dest->ebp = src->ebp;
    dest->esp = src->esp;
    dest->eip = src->eip;
} 

/**
 * @brief Copy the execution content from one process
 * to another. The content include all the register
 * values, the address space content. Helper function
 * used by fork.
 *
 * @param dest The destination process control block
 *
 * @param src The source process control block
 *
 * @return Void
 */
void do_fork_process(process_t* dest,process_t* src){
	fork_context(&dest->context,&src->context);
	clone_pagetable(&dest->ptable,&src->ptable);
}

/**
 * @brief Create a new process control block, and
 * make a copy of existing process control block
 *
 * @param process The process control block to be
 * copied.
 *
 * @return The newly created control block
 */
process_t *fork_process(process_t* process){

	process_t *child = alloc_pcb();
	init_pagetable(&child->ptable);
	copy_kernel_vm(&child->ptable);
	do_fork_process(child,process);
	child->parent = process;
	child->status = RUNNABLE;

	return child;
}

/**
 * @brief Create a new thread. The newly created
 * thread shares the same address space of its
 * parent.
 *
 * @param parent The parent process control block.
 *
 * @return The newly forked thread
 */
process_t *thread_fork(process_t *parent){
    process_t *thread = alloc_pcb_without_va();
    thread->parent = parent;
	/* As thread shares the address space with parent, we 
	 * only need to set the address spaec to parent.
	 */
    thread->ptable.page_directory = parent->ptable.page_directory;
	fork_context(&thread->context,&parent->context);

	thread->status = RUNNABLE;
    return thread;
}

/**
 * @brief Set up the execution environment for the new scheduled
 * process. Including the page table directory and the returning
 * kernel stack.
 *
 * @param The process to be scheduled to be executed
 *
 * @return Void
 */
void load_context(process_t *process){
	set_cr3((int)(process->ptable).page_directory);
	set_esp0((unsigned int)process->kstack);
}

/**
 * @brief Process/Thread schedule. Use round robin
 * schedule strategy.
 *
 * @return Void
 */
void schedule(){
	int found = 0;
	int search_counter = 0;
	
	/* We use current_scheduled as the starting point */
	int index = current_scheduled;
	process_t *next_process=NULL;

	spin_acquire(&lock);
	/* Search at most MAX_PROCESS times*/
	while(search_counter<MAX_PROCESS){
		/* Check whether the next slot is RUNNABLE */
		if((processes[index].status==RUNNABLE)
				&&(current!=&processes[index])){
			found = 1;
			next_process = &processes[index];
			/* Cover the corner case. Only current is not NULL,
			 * and the status is RUNNING that change the status
			 * to be RUNNABLE
			 */
//lprintf("FROM %d To %d", next_process->pid, current->pid);
			if((current!=NULL)&&(current->status==RUNNING)){
				current->status = RUNNABLE;
			}
			next_process->status = RUNNING;
			break;
		}
		index = (index+1)%MAX_PROCESS;
		search_counter++;
	}

	/* Update the searching index for the next time */
	current_scheduled = (index+1)%MAX_PROCESS;
	spin_release(&lock);

	if(found){
		current = next_process;
	}

	/* Set up the execution environment for the scheduled process */
	load_context(current);
}

/**
 * @brief Set the exit value for current Process/Thread
 *
 * @param status The status value to be set
 *
 * @return Void
 */
void sys_set_status(int status){

	spin_acquire(&lock);
	if(current!=NULL){
		current->exit_code = status;
	}
	spin_release(&lock);
}

/**
 * @brief Exit current process, if parent is waiting,
 * wake them up. Schedule to the next available process.
 *
 * @return Void
 */
void sys_vanish(){
	spin_acquire(&lock);
	current->status = ZOMBIE;
	/* If parent is waiting, wake it up */
	if(current->parent!=NULL){
		if(current->parent->status == BLOCKING){
			current->parent->status = RUNNABLE;
		}
	}
	spin_release(&lock);
	/* Schedule to the next available process */
	schedule_and_switch(&current->context);
}

/**
 * @brief Wait until one of its children exit.
 * If there are no children or some children already
 * exited, return immediate.
 *
 * @param status The exist status of the existed child
 *
 * @return The existed child thread id or -1 if there 
 * is no child thread
 *
 */
int sys_wait(int* status){
	int index,child_id;
	process_t *waiting = current;

	while(1){
		index = 0;
		child_id = -1;
		/* Check whether there are children */
		for(;index<MAX_PROCESS;index++){
			if(processes[index].parent==waiting){

				child_id = processes[index].pid;
				if(processes[index].status == ZOMBIE){
					child_id = processes[index].pid;
					*status = processes[index].exit_code;
					/*Recycle zombie resource and reset status*/
					processes[index].status = FREE;
					return child_id;
				}
			}
		}
		
		/* No children, return immediate */
		if(child_id == -1){
			return -1;
		}
		/* There are running children */
		waiting->status = BLOCKING;
		/* Schedule to the next process immediately */
		schedule_and_switch(&waiting->context);
	}
}

/*
 * @brief Helper funtion used to dup a string.
 * It is used to copy strings in user space
 * to kernel space. It is useful to copy strings
 * between two address spaces.
 *
 * @param str The String pointer to be duplicated
 *
 * @return The duplicated string. It uses _malloc,
 * and it should be freed.
 */
char *_strdup(char * str){
    int length = strlen(str);
    char *result = (char*)_malloc(strlen(str)+1);

    int index = 0;
    while(index<length){
        result[index]=str[index];
        index++;
    }

	result[length]='\0';
    return result;
}

/**
 * @brief Load a new image specified by the path
 * and execute it. We set up a new pagetable,
 * load the image content to the new pagetable,
 * setup the related stacks, and replace with the
 * new pagetable for current process.
 *
 * @param path The executable path
 *
 * @param argv The arguments passed to the executable
 *
 */
int exec(char *path,char **argv){

	 switch_to_uvm();
	/* Check whether the elf exists or not */
	if(!elf_exists(path)){
		return -1;
	}

	/* Switch to kernel address space. As
	 * we need to access the physical memory
	 * to set up the page directory.
	 */
	switch_to_kvm();
    pagetable_t new_ptable, temp_ptable;

	/* Init the new pagetable */
    init_pagetable(&new_ptable);
    copy_kernel_vm(&new_ptable);
    setup_stack(&new_ptable);

	/* As the parameter is passed on the user space stack*/
	switch_to_uvm();
    int index = 1;
    int sp = STACK_TOP;
    char* dup;
    char* args[MAX_ARGS];

	/*update the stack pointer */
    sp = ALIGN(sp - (strlen(path) + 1));
    args[0]=(char*)sp;
    dup = _strdup(path);
	/*copy the path argument to the new pagetable*/
    copy_out(&new_ptable,(void*)sp,dup,(strlen(path) + 1));
	_free(dup);

    int argc = 0;
	/* Copy all the arguments to the address space.
	 * The arguments are copied to the stack.
	 */
    while(argv[argc]){
        sp = ALIGN(sp - (strlen(argv[argc]) + 1));
        dup = _strdup(argv[argc]);
        copy_out(&new_ptable,(void*)sp,dup,(strlen(argv[argc]) + 1));
        _free(dup);
        args[index]=(char*)sp;
        argc++;
        index++;
    }

	/* After copy the arguments, adjust the final stack 
	 * point, and copy the args to the new stack
	 */
    sp = sp - 4*index;
    copy_out(&new_ptable,(void*)sp,args,4*index);
    process_t *proc = current;

    temp_ptable.page_directory = proc->ptable.page_directory;
    proc->ptable.page_directory = new_ptable.page_directory;

    /* Reset stack point address */
    proc->context.ebp = STACK_TOP;
    proc->context.esp = sp;
	path = _strdup(path);

	/* Switch to kernel address space as load_elf need to 
	 * access kernel address space.
	 */
	switch_to_kvm();
    int entry = load_elf(&new_ptable,path);
    (proc->context).eip = entry;
    proc->status = RUNNABLE;
	
	/* Free the duplicated path */
	_free(path);

	return 0;
}

/**
 * @brief If the status is zero, wait until
 * make_runnable wakes it up.
 *
 * @param status The status value specifies
 * whether this thread should wait or not
 *
 * @return 0 if success, -1 otherwise
 */
int sys_deschedule(int *status){
	spin_acquire(&lock);
	process_t *descheduled = current;

	if(!*status){
		descheduled->status = BLOCKING;
		spin_release(&lock);
		/* Schedule to the next available process */
		schedule_and_switch(&descheduled->context);
	}else{
		spin_release(&lock);
		return -1;
	}

	return 0;
}

/**
 * @brief Turn the specified thread into runnable
 *
 * @param tid The specified thread id
 *
 * @return If the specified thread exits, and
 * is BLOCKING, return 0; otherwise -1; 
 */
int sys_make_runnable(int tid){
	spin_acquire(&lock);

	int result=-1, i=0;
	for(;i<MAX_PROCESS;i++){
		if(processes[i].pid ==tid
				&&processes[i].status==BLOCKING){
			result = 0;
			processes[i].status=RUNNABLE;
			break;
		}
	}

	spin_release(&lock);

	return result;
}

/**
 * @brief Schedule to a specified process
 *
 * @param process The specified process pointer
 *
 * @return Void
 *
 */
void switch_to(process_t *process){
	current->status = RUNNABLE;

	current = process;
	current->status = RUNNING;
	load_context(current);
}

/**
 * @brief Give up self to another thread. If the specified
 * thread exists and is runnable, swith to the thread.
 * Otherwise, return immediately.
 *
 * @param tid The thread id
 *
 * @return 0 if the specified id exists, -1 otherwise
 *
 *
 */
int sys_yield(int tid){
	process_t *self = current;

    /* If tid is -1, let scheduler choose 
     * the next running thread */
    if(tid == -1){
        schedule_and_switch(&self->context);
        return 0;
    }

    int i=0;
	spin_acquire(&lock);
    for(;i<MAX_PROCESS;i++){
        if(processes[i].pid ==tid
                &&processes[i].status==RUNNABLE){
            spin_release(&lock);
			schedule_and_switch(&self->context);
            return 0;
        }
    }

    spin_release(&lock);
    return -1;
}

/**
 * @brief Helper function used to refresh the remaining
 * ticks of a sleeping process. If the sleeping ticks
 * is decreased to be zero, wake it up.
 *
 * @return Void
 */
void refresh_sleeping_ticks(){
	int i=0;
    spin_acquire(&lock);
    for(;i<MAX_PROCESS;i++){
		if(processes[i].status == BLOCKING){
			if(processes[i].wait_ticks>=1){
				processes[i].wait_ticks--;
				if(processes[i].wait_ticks==0){
					processes[i].status = RUNNABLE;
				}
			}
		}
    }
    spin_release(&lock);
}

/**
 * @brief Put a process into sleep. The argument 
 * check is done in the system call handler.
 *
 * @param ticks The sleeping ticks/
 *
 * @return Void
 */
void sys_sleep(int ticks){
	process_t *self = current;
	self->status = BLOCKING;
	self->wait_ticks = ticks;

	schedule_and_switch(&self->context);
}
