#include <x86/cr.h>
#include <stdlib.h>
#include <malloc_internal.h>
#include <simics.h>
#include "vm.h"
#include "types.h"
#include "queue.h"
#include "spinlock.h"
#include "pagetable.h"
#include "process.h"
#include "common_kern.h"

/* The total number of the physical frames */
int total_pages = 0;
int free_pages = 0;

/* The spinlock used to guard the current memory operations */
spin_t lock;
/* Initial pagetable for kernel start up */
pagetable_t pagetable;

queue_t free_memories;
heap_page_t head={0};

/**
 * @brief switch to kernel virtual address space
 *
 */
void switch_to_kvm(){
	set_cr3((int)pagetable.page_directory);
}

/**
 * @brief switch to user virtual address space
 *
 */
void switch_to_uvm(){
	if(current!=NULL){
		set_cr3((int)current->ptable.page_directory);
	}
}

/**
 * @brief Init kernel virtual meory
 *
 */
void init_kernel_vm(pagetable_t *ptable){
	int index = 0;

	/* Direct map virtual address to physical address */
	while(index < total_pages){
		map_page_with_mask(ptable,
				(void*)(index*PAGE_SIZE),(void *)(index*PAGE_SIZE),KERNEL_MASK);
		index++;
	}
}

/* Copy kernel virtual memory mapping */
void copy_kernel_vm(pagetable_t * ptable){
	int index = 0;
	while(index<KERNEL_VM_PDE_NUM){
		ptable->page_directory[index]=pagetable.page_directory[index];
		index++;
	}
}

/**
 * @brief When kernel starts up, set up the virtual
 * memory for it.
 *
 * @return Void
 *
 */
void setup_memory(){
	spin_init(&lock);
	total_pages = machine_phys_frames();
	queue_init(&free_memories);
	
	unsigned int pa = USER_MEM_START;
	while(pa<total_pages*PAGE_SIZE){
		enqueue(&free_memories,(void*)pa);
		pa += PAGE_SIZE;
		free_pages++;
	}
	init_pagetable(&pagetable);
	init_kernel_vm(&pagetable);
	switch_to_kvm();
}


/** 
 * @brief Get free physical frame from 
 * the memory queue
 *
 * @return The start address of the physical
 * frame.
 */
int get_free_frame(){
	int address = 0;

	spin_acquire(&lock);
	void *data = dequeue(&free_memories);
	free_pages--;
	spin_release(&lock);

	if(data==NULL){
		address = -1;
	}else{
		address = (int)data;
	}

	return address;
}

int get_free_frame_number(){
	return free_pages;
}

/**
 * @brief Release physical frame to the free
 * memory queue
 *
 * @param pa The physical frame address
 *
 * @return Void
 */
void release_physical_frame(void *pa){
	spin_acquire(&lock);
	enqueue(&free_memories,pa);
	free_pages++;
	spin_release(&lock);
}


void setup_stack(pagetable_t* ptable){
    map_virtual_address_with_mask(ptable,
			(void*)(STACK_TOP-3*PAGE_SIZE),4*PAGE_SIZE,USER_RW_MASK);
}   

void zero_page(void *page){
	int index = 0;
	char *base = (char*)page;

	while(index<PAGE_SIZE){
		base[index++] = (char)0;
	}
}


/**
 * @brief Map a block of virtual address space to
 * free physical memory.
 *
 *
 * @return Void
 *
 */
void map_virtual_address_with_mask(pagetable_t *ptable,void *va,int length,int mask){
	unsigned int orig_va = (int)va;
    
    if(!(((unsigned int)va)%PAGE_SIZE)){
        va = (void*)((unsigned int)va/PAGE_SIZE*PAGE_SIZE);
        length += orig_va - (unsigned int)va;
    }   
        
    int free_frame;
    while(length>0){
        free_frame = get_free_frame();
		//lprintf("map page: va %p -> pa %p",va,(void*)free_frame);
        map_page_with_mask(ptable,va,(void*)free_frame,mask);
		zero_page((void*)free_frame);
        length -= PAGE_SIZE;
        va = (void*)((int)va+PAGE_SIZE);
    }
}

void set_virtual_address_mask(pagetable_t *ptable,void *va,int length,int mask){
	unsigned int orig_va = (int)va;

	if(!(((unsigned int)va)%PAGE_SIZE)){
        va = (void*)((unsigned int)va/PAGE_SIZE*PAGE_SIZE);
        length += orig_va - (unsigned int)va;
    }

	while(length>0){
		set_page_mask(ptable,va,mask);
		length -= PAGE_SIZE;
		va = (void*)((int)va+PAGE_SIZE);
	}
}

void map_virtual_address(pagetable_t *ptable,void *va,int length){
	map_virtual_address_with_mask(ptable,va,length,LOW_MASK);
}

/**
 * @brief Check whether the newly allocated heap is overlapped
 * with existing heap pages. Help function used by new_pages()
 *
 * @param head The pointer to the allocated heap list
 *
 * @param heap_start Start address of the newly allocated heap
 *
 * @param heap_end End address of the newly allocated heap
 *
 * @return 1 if the newly allocated heap is overlapped with
 * existing heaps, 0 otherwise.
 *
 */
int is_overlaped_heap(heap_page_t *head,unsigned int heap_start,unsigned int heap_end){
	int result = 0;

	while(head){
		if(!(head->start>heap_end||heap_start>head->end)){
			result = 1;
			break;
		}
		head = head->next;
	}
	return result;
}

/**
 * @brief Check whether the heap address is valid or not.
 * Help function used in page fault handler
 *
 * @param head The pointer to the allocated heap list
 *
 * @param address The heap address
 *
 * @return 1 if the address is valid, 0 otherwise
 *
 */
int is_valid_heap_address(heap_page_t *head,int address){
	int result = 0;

	while(head){
		if(head->start>address){
			break;
		}else if(address<=head->end){
			result = 1;
			break;
		}
		head = head->next;
	}

	return result;
}

/**
 * @brief create a new heap page node with start_address start
 * and end_address end
 *
 * @param start The start address of the new heap node
 * @param end The end address of the new heap node
 *
 * @return Pointer to the newly created page node
 */
heap_page_t *new_heap_page_node(int start,int end){
	heap_page_t *result = (heap_page_t*)_malloc(sizeof(heap_page_t));
	
	result->end = end;
	result->start = start;
	result->next = NULL;

	return result;
}

/**
 * @brief Copy the content value from kernel mode to user address
 * space of another process
 *
 * @param ptable The pointer of the pagetable directory of 
 * another process
 * @param va The user mode memory address
 * @param pa The kernel mode memory address
 * @param length The content length to be copied
 *
 * @return Void
 */
void copy_out(pagetable_t *ptable,void* va,void* pa,int length){
	int old_cr3 = get_cr3();
	set_cr3((int)(ptable->page_directory));
	
	int index = 0;
	while(index<length){
		((char*)va)[index]=((char*)pa)[index];
		index++;
	}
	
	set_cr3(old_cr3);
}

/**
 * @brief Add newly allocated heap address to heap list.
 * Help function used in new_pages.
 *
 * @param head The pointer to the allocated heap list
 * @param start The start address of the new heap area
 * @param end The end address of the new heap area
 *
 * @return Void
 */
void add_heap_address(heap_page_t *head,int start,int end){
	/* No element in list */
	if(head->start==-1){
		head->start = start;
		head->end = end;
		return;
	}

	heap_page_t *prev;
	heap_page_t *cur;
	heap_page_t * new_node;

	prev=NULL;
	cur=head;
	while(cur&&(cur->end<start)){
		prev = cur;
		cur = cur->next;
	}

	if(prev){
		new_node = new_heap_page_node(start,end);
		prev->next = new_node;
		new_node->next = cur;
	}else{
		new_node = new_heap_page_node(head->start,head->end);
		new_node->next=head->next;
		head->next=new_node;
		head->start = start;
		head->end = end;
	}
}

int validate_params(heap_page_t *head,void *address, int len){
    process_t *self = current;

    if((unsigned int)len>(unsigned int)free_pages*(unsigned int)PAGE_SIZE){
        return 0;
    }

    int is_valid = !is_overlaped_heap(head,(unsigned int)address,len)
        &&!(((unsigned int)(address))%PAGE_SIZE)
        &&(len>0)&&!(len%PAGE_SIZE);

    if(!is_valid){
        return 0;
    }

    unsigned int start = (unsigned int)address;

	while(len>0){
        if(is_mapped_va(&self->ptable,(void*)start)){
            return 0;
        }
        start += PAGE_SIZE;
		len -= PAGE_SIZE;
    }

    return 1;
}



/**
 * @brief alloc new physical memory to calling process
 * add map it to specific virtual address
 *
 * @param addr The starting virtual address
 *
 * @param len The len of the newly allocated address
 *
 * @return 0 If allocate successfully, 1 otherwise.
 *
 */
int sys_new_pages(void * addr, int len){
	if(validate_params(&head,addr,len)){
		map_virtual_address_with_mask(&current->ptable,addr,len,USER_RW_MASK);
		add_heap_address(&head,(unsigned int)addr,((unsigned int)addr)+(unsigned int)(len-1));
		return 0;
	}
	return -1;
}

int sys_remove_pages(void* base){
	int addr = (int)base;
	
	/* The user stack is empty  */
	if(head.start==-1){
		return -1;
	}
	
	heap_page_t *prev;
	heap_page_t *cur;
	heap_page_t *target;
	
	prev=NULL;
	cur = &head;
	
	while(cur&&(cur->start<addr)){
		prev = cur;
		cur = cur->next;
	}
	
	if(cur!=NULL&&cur->start==addr){
		target = cur;
		prev->next = cur->next;

		int pa = 0;
		while(addr<cur->end){
			pa = unmap_page(&current->ptable,(void*)addr);
			if(pa!=0){
				release_physical_frame((void*)pa);
			}
			addr += PAGE_SIZE;
		}

		return 0;
	}
	return -1;
}
