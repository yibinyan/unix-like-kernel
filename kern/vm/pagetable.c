/**@file pagetable.c
 *
 * @brief Functions to init page directory, page table
 * and map virtual address of one process to physical
 * address.
 *
 * 				32 bits virtual address
 * ------------------------------------------------
 * |  xxxxxxxxxx  |  xxxxxxxxxx  |  xxxxxxxxxxxx  |
 * ------------------------------------------------
 * | pde offset   |  pte offset  | physical offset|
 * ------------------------------------------------
 *     10 bits        10 bits          12 bits
 */
#include <pagetable.h>
#include <x86/cr.h>
#include <stdlib.h>
#include <simics.h>
#include "malloc_internal.h"
#include "asms.h"
#include "common_kern.h"
#include "vm.h"

/* Helper function used to get the leading 10 bits of
 * a virtual memory. The pde offset in page directory
 */
#define GET_PDE(va) (((unsigned int)(va))&0xFFC00000)>>22

/* Helper function used to get the middle 10 bits of
 * a virtual memory. The pte offset in page table
 */
#define GET_PTE(va) (((unsigned int)(va))&0x3FF000)>>12

/* Low 12 bits mask to recover the pte */
#define LOW_UNMASK 0xFFFFF000

//extern int get_free_frame();

/**
 * @brief Initialize the pagetable_t variable, allocate
 * 1K PDE in the page directory. 4K memory
 *
 * @param ptable Pointer to the pagetable_t variable
 *
 * @return Void
 */
void init_pagetable(pagetable_t *ptable){
	int count = 0;
	/* Allocate PTE_COUNT page directory entity for page directory */
	ptable->page_directory = 
		(int **)get_free_frame();
	
	/* All the PDE will point to NULL */
	while(count<PTE_COUNT){
		ptable->page_directory[count] = NULL;
		count++;
	}
}

/**
 * @brief Release the physical frame pointered
 * by page table entry pte
 *
 * @param pte Page table entry pointer
 *
 * @return Void
 */
void release_pagetable_entry(int pte){
	if(pte==0){
        return;
    }   
    
    int pa = pte&LOW_UNMASK;
    release_physical_frame((void*)pa);
}

/**
 * @brief Release all the physical frames pointered
 * by page directy entry pde
 *
 * @param pde Page directory entry pointer
 *
 * @return Void
 */
void release_pagedir_entry(void *pde){
	int i = 0;
    for(;i<PTE_COUNT;i++){
		release_pagetable_entry(((int*)pde)[i]);
		((int*)pde)[i]=0;
    }
    //Todo Release PDE
}

/**
 * @brief Release physical frames allocated to 
 * process's user space.
 *
 * @param ptable Pointer to the pagetable_t variable, which
 * contains the paging information for one process
 *
 * @return Void
 *
 */
void release_allocated_user_space_pf(pagetable_t *ptable){
	void *pde;
	int index;
	
	index = KERNEL_VM_PDE_NUM -1;
	for(;index<PTE_COUNT;index++){
		if(ptable->page_directory[index]!=NULL){
			pde = (void*)((int)ptable->page_directory[index]&LOW_UNMASK);
			release_pagedir_entry(pde);
			ptable->page_directory[index]=NULL;
		}
	}
}


/**
 * @brief Map virtual address to physical address
 *
 * @param ptable Pointer to the pagetable_t variable, which
 * contains the paging information for one process
 *
 * @param va The virtual memory address
 * 
 * @param pa The physical memory address
 *
 * @return Void
 */
void map_page_with_mask(pagetable_t *ptable,void *va,void *pa,int mask){
	unsigned int pde = GET_PDE(va);
	unsigned int pte = GET_PTE(va);

	/* If the page table is empty, allocate a new one */
	if(ptable->page_directory[pde]==NULL){
		ptable->page_directory[pde] = 
			(int*) get_free_frame();
		
		/* At first, all page table entries are invalid */
		int i = 0;
		for(;i<PTE_COUNT;i++){
			ptable->page_directory[pde][i] = 0;
		}
		/* Update PDE status bits. For CP1, we enable all status.
		 * Will be changed later on
		 */
		ptable->page_directory[pde] = 
			(int*)((int)(ptable->page_directory[pde])|LOW_MASK);
	}

	int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);

	/* Whether this page is mapped or not, map a new 
	 * physical frame to it. Logic should be added
	 * to release previously allocated physical frame.
	 */
	orig_pde[pte]=((int)pa)|mask;
}

void map_page(pagetable_t *ptable,void *va,void *pa){
	map_page_with_mask(ptable,va,pa,LOW_MASK);
}

void set_page_mask(pagetable_t *ptable,void *va,int mask){
	unsigned int pde = GET_PDE(va);
    unsigned int pte = GET_PTE(va);

	if(ptable->page_directory==NULL){
		return;
	}

    /* If the page table is empty, just return */
    if(ptable->page_directory[pde]==NULL){
		return;
    }

    int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);

    /* If the pte is mapped, change the mask*/ 
	if(orig_pde[pte]!=0){
		orig_pde[pte]=(orig_pde[pte]&LOW_UNMASK)|mask;
	}
}

int get_page_mask(pagetable_t *ptable,void *va){
	int result = -1;
	unsigned int pde = GET_PDE(va);
    unsigned int pte = GET_PTE(va);

    /* If the page table is empty, just return */
    if(ptable->page_directory==NULL||
			ptable->page_directory[pde]==NULL){
        return result;
    }

	int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);

	if(orig_pde[pte]!=0){
		result = orig_pde[pte]&0x7;
	}

	return result;
}

/**
 * @brief Check whether one specified virtual memory
 * is mapped to a physical frame or not.
 *
 * @param ptable The pagetable_t pointer to the
 * virtual address space.
 *
 * @param va The virtual address
 *
 * @return 1 if the virtual address is mapped,
 * otherwise 0
 */
int is_mapped_va(pagetable_t *ptable,void *va){
	unsigned int pde = GET_PDE(va);
	unsigned int pte = GET_PTE(va);

	/* The mapped virtual memory should have valid pde */
	if(ptable->page_directory[pde]!=NULL){
		int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);
		/* Mapped virtual memory should also have valid pte */
		if(orig_pde[pte]!=0){
			return 1;
		}
	}

	return 0;
}

/**
 * @brief Get physical frame address from virtual address
 *
 * @param ptable The pagetable_t pointer to the virtual
 * address space
 *
 * @param va The virtual address
 *
 * @return 0 if the virtual address is not mapped,
 * otherwise return the corresponding physical address
 *
 */
int get_pa_from_va(pagetable_t *ptable,void *va){
	unsigned int pde = GET_PDE(va);
    unsigned int pte = GET_PTE(va);

    /* The mapped virtual memory should have valid pde */
    if(ptable->page_directory[pde]!=NULL){
        int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);
        /* Mapped virtual memory should also have valid pte */
        if(orig_pde[pte]!=0){
            return orig_pde[pte]&LOW_UNMASK;
        }
    }

    return -1;
}

/**
 *
 * @brief Unmap one page_size of memory from user spave
 *
 * @param ptable The pagetable object of user process
 * @param va The starting address of the PAGE to be unmapped
 *
 * @return The physical address mapped to the virtual address.
 * If no physical address mapped, return 0
 *
 */
int unmap_page(pagetable_t *ptable,void *va){
	unsigned int result = 0;
	unsigned int pde = GET_PDE(va);
	unsigned int pte = GET_PTE(va);

	if(ptable->page_directory[pde]!=NULL){
        int *orig_pde = (int*)((int)ptable->page_directory[pde]&LOW_UNMASK);
        /* Mapped virtual memory should also have valid pte */
        if(orig_pde[pte]!=0){
			result = orig_pde[pte]&LOW_UNMASK;
            orig_pde[pte] = 0;
        }
    }

	return result;
}

/**
 * @brief Release allocated physical frames in process's
 * page directory
 *
 * @param ptable The pagetable_t pointer to the virtual
 * address space
 *
 * @return Void
 */
void release_physical_frames(pagetable_t *ptable){
	unsigned long va = USER_MEM_START;
	unsigned long max_va = (1L)<<31;

	while(va<max_va){
		int pa = get_pa_from_va(ptable,(void*)va);
		
		/* If the corresponding physical frame is
		 * allocated, return to free memory pool.
		 */
		if(pa!=-1){
			release_physical_frame((void*)pa);
		}
        va +=PAGE_SIZE;
    }

}

/**
 * @brief Copy data between two blocks of memory.
 *
 * @param dest The start address of the destination
 * address.
 *
 * @param src The start address of the source 
 * address
 *
 * @return Void
 *
 */
void mem_copy(void *dest,void * src,int length){
	int index = 0;

	char *from = (char *)src;
	char *to = (char *)dest;
	
	while(index<length){
		to[index] = from[index];
		index++;
	}
}


/**
 * @brief Copy the virtual address space from src to
 * dest. This function is used the fork system call
 *
 * @param dest. The pagetable_t pointer to the 
 * destination address space.
 *
 * @param src. The pagetable_t pointer to the source
 * address space.
 *
 * @return Void
 */
void clone_pagetable(pagetable_t *dest,pagetable_t *src){
	int pa = 0;

	/* Only map the memory from USER_MEM_START 
	 * to the highest virtual memory address
	 */
	unsigned long va = VA_TOP;
	int mask = -1;

	/**/
	while(va>=USER_MEM_START){
		/* Only mapped address should be copied */
		if(is_mapped_va(src,(void*)va)){
			/* Allocate a free physical frame to copy
			 * the memory content
			 */
			pa = get_free_frame();

			/* Get the physical frame from source va */
			void * src_pa = (void*)get_pa_from_va(src,(void*)va);
			/* Clone content from source to destination */
			mem_copy((void*)pa,src_pa,PAGE_SIZE);
			/* After copying the content, we should
			 * map it to destination's address space.
			 */
			mask = get_page_mask(src,(void*)va);
			if(mask ==-1){
				mask = USER_RW_MASK;
			}
			map_page_with_mask(dest,(void *)va,(void*)pa,mask);
		}
		va -= PAGE_SIZE;
	}
}
