#include "gates.h"

void set_user_trap_gate(struct gate *target, void *wrapper){
    uint32_t low_part = (uint32_t)wrapper & (0xffff);
    uint32_t high_part = (uint32_t)wrapper & (0xffff << 16);
     
    target->last_32bits = (SEGSEL_KERNEL_CS << 16) | low_part;
    target->first_32bits = high_part | OTHER_BITS_TRAP_USER;
}

void set_user_interrupt_gate(struct gate *target, void *wrapper){
    uint32_t low_part = (uint32_t)wrapper & (0xffff);
    uint32_t high_part = (uint32_t)wrapper & (0xffff << 16);
     
    target->last_32bits = (SEGSEL_KERNEL_CS << 16) | low_part;
    target->first_32bits = high_part | OTHER_BITS_INTERRUPT_USER;
}

void set_kernel_interrupt_gate(struct gate *target, void *wrapper){
    uint32_t low_part = (uint32_t)wrapper & (0xffff);
    uint32_t high_part = (uint32_t)wrapper & (0xffff << 16);
     
    target->last_32bits = (SEGSEL_KERNEL_CS << 16) | low_part;
    target->first_32bits = high_part | OTHER_BITS_INTERRUPT_KERNEL;
}

void set_kernel_trap_gate(struct gate *target, void *wrapper){
    uint32_t low_part = (uint32_t)wrapper & (0xffff);
    uint32_t high_part = (uint32_t)wrapper & (0xffff << 16);
     
    target->last_32bits = (SEGSEL_KERNEL_CS << 16) | low_part;
    target->first_32bits = high_part | OTHER_BITS_TRAP_KERNEL;
}
