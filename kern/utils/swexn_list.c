/**
 * @file swexn_list.c
 *
 * @brief A linked list to store content related to software interrupt of each thread. 
 * It is not thread safe.
 *
 */

#include <swexn_list.h>
#include <stdlib.h>
#include "malloc_internal.h"
#include <simics.h>

list_t *head;
list_t *tail;

/**
 *	@brief delete node from list by tid
 *
 *	@param queue The pointer to the queue
 *
 *	@return 1 if successful. 0 if fail.
 */
int delete_from_swexn_list(int tid){
	list_t *temp_head = head;
    list_t *before;
    int result = -1;    

    while (temp_head->next != NULL){
        before = temp_head;
        temp_head = temp_head->next;
        
        if (temp_head->tid == tid){
            before->next = temp_head->next;

            if (before->next == NULL){
                tail = before;
            }
            
            _free(temp_head->content);
            _free(temp_head);
            result = 0;
            break;
        }
    }

	return result;
}

/**
 *	@brief delete node from list by tid
 *
 *	@param queue The pointer to the queue
 *
 *	@return 1 if successful. 0 if fail.
 */
swexn_node* get_from_swexn_list(int tid){
	list_t *temp_head = head;
    swexn_node *result = NULL;    

    while (temp_head != NULL){
        if (temp_head->tid == tid){
            result = temp_head->content;
        }

        temp_head = temp_head->next;
    }

	return result;
}

/**
 *	@brief Check whether the list is empty or not.
 *
 *	@return 1 if the list is empty, otherwise 0
 */
int is_swexn_list_empty(){
	return (head->next==NULL);
}


void copy_ureg_to_userspace(ureg_t *dst, ureg_t *src){
        dst->cause = src->cause;
        dst->cr2 = src->cr2;   /* Or else zero. */
 
        dst->ds = src->ds;
        dst->es = src->es;
        dst->fs = src->fs;
        dst->gs = src->gs;
    
        dst->edi = src->edi;
        dst->esi = src->esi;
        dst->ebp = src->ebp;
        dst->zero = 0;  /* Dummy %esp, set to zero */
        dst->ebx = src->ebx;
        dst->edx = src->edx;
        dst->ecx = src->ecx;
        dst->eax = src->eax;

        dst->error_code = src->error_code;
        dst->eip = src->eip;
        dst->cs = src->cs;
        dst->eflags = src->eflags;
        dst->esp = src->esp;
        dst->ss = src->ss;
}

/**
 *	@brief Add a elment to the list.
 *
 *	@param tid  tid of process
 *	@param esp3 begin address of exception stack
 *	@param eip  swexn_handler
 *  @param arg  argument
 *  @param ureg ureg
 *
 *	@return void
 */
void add_to_swexn_list(int tid, void *esp3, void *eip, void *arg){
    swexn_node *content = get_from_swexn_list(tid);

    if (content == NULL){
        list_t *node = (list_t *)_malloc(sizeof(list_t));
        node->tid = tid; 
    
        node->content = (swexn_node *)_malloc(sizeof(swexn_node));
        node->content->esp3 = esp3;
        node->content->eip = eip;
        node->content->arg = arg;
        node->next = NULL;
        
        tail->next = node;
        tail = node;
    }else{
        content->esp3 = esp3;
        content->eip = eip;
        content->arg = arg;
    }
}

/**
 * @brief Init the list as empty
 *
 * @param head of list
 *
 * @return Void
 */
void swexn_list_init(){
	head = (list_t *)_malloc(sizeof(list_t));
    tail = head;
}

/**
 * @brief Free the dynamically allocated memory
 * in the list structure
 *
 * @return Void
 *
 */
void swexn_list_destory(){
    list_t *temp = head;
    list_t *next_temp;

	while(temp!=tail){
        next_temp = temp->next;
        _free(temp->content);
        _free(temp);
		temp = next_temp;
	}

    _free(temp->content);
    _free(temp);
	
    head = NULL;
	tail = NULL;
}

