/**
 * @file queue.c
 *
 * @brief A general purpose queue. It is not thread safe.
 * Data is stored as void* in queue_el_t structure.
 *
 */

#include <queue.h>
#include <stdlib.h>
#include "malloc_internal.h"

/**
 *	@brief Return the head of the queue, if the queue
 *	is empty, return NULL
 *
 *	@param queue The pointer to the queue
 *
 *	@return The head of the queue. NULL if queue is
 *	empty
 */
void *dequeue(queue_t* queue){
	void *result=NULL;
	queue_el_t *ele = NULL;

	/* If the queue is empty return NULL*/
	if(is_queue_empty(queue)){
		return NULL;
	}else{
		/* There is only one element in queue */
		ele = queue->head;
		if(queue->head==queue->tail){
			result = queue->head->value;
			queue->head = NULL;
			queue->tail = NULL;
		}else{
			result = queue->head->value;
			queue->head = queue->head->next;
		}
	}

	/* Free the node element */
	free(ele);

	return result;
}

/**
 *	@brief Check whether a specified queue is empty or not.
 *	It is used internally, we will not lock the queue.
 *
 *	@param queue The pointer to the queue
 *
 *	@return True if the queue is empty, otherwise false
 */
int is_queue_empty(queue_t *queue){
	return (queue->tail==NULL) && (queue->head==NULL);
}

/**
 *	@brief Add a queue elment to the queue's header.
 *
 *	@param queue The queue pointer
 *	@param el The adding element
 *
 *	@return Void
 */
void enqueue(queue_t *queue, void *data){
	queue_el_t *ele = (queue_el_t *)_malloc(sizeof(queue_el_t));
	ele->value = data;
	ele->next = NULL;

	if(is_queue_empty(queue)){
		queue->head = ele;
		queue->tail = ele;
	}else{
		queue->tail->next=ele;
		queue->tail = ele;
	}
}

/**
 * @brief Init the queue as empty
 *
 * @param queue Pointer to the queue
 *
 * @return Void
 */
void queue_init(queue_t *queue){
	queue->head = NULL;
	queue->tail = NULL;
}

/**
 * @brief Free the dynamically allocated memory
 * in the queue structure
 *
 * @param queue Pointer to the queue
 *
 * @return Void
 *
 */
void queue_destory(queue_t *queue){
	queue_el_t *head = queue->head;
	queue_el_t *temp;

	while(head!=NULL){
		temp = head->next;
		free(head);
		head = temp;
	}

	queue->head = NULL;
	queue->tail = NULL;
}

/**
 * @brief Free the dynamically allocated memory
 * in the queue structure, also frees the containing data
 *
 * @param queue Pointer to the queue
 * @param release The memory release function passed in by the user
 *
 * @return Void
 *
 */
void queue_destory_with_data(queue_t *queue, void(*release)(void*)){
	queue_el_t *head = queue->head;
	queue_el_t *temp;

    while(head!=NULL){
		release(head->value);
        temp = head->next;
		free(head);
        head = temp;
    }   
    
    queue->head = NULL;
    queue->tail = NULL;
}

