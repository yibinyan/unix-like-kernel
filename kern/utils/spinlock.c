#include "spinlock.h"
#include "asms.h"

/**
 *  @brief spin lock initialization
 *
 *  @param spin Pointer to the spin lock
 *  to be initialized
 *
 *  @return Void
 */
void spin_init(spin_t *spin){
    xchg(&spin->lock,1);
}

/**
 * @brief If the spin lock is not occupied by
 * others, obtain this spin lock
 * 
 * @param spin The pointer to the spin lock
 *
 * @return Void
 */
void spin_acquire(spin_t *spin){
    int val = xchg(&spin->lock,0);

    while(!val){
        pause();
        val = xchg(&spin->lock,0);
    }
}

/**
 * @brief Release an occupied lock
 *
 * @param spin The pointer to the spin lock
 *
 * @return Void
 */
void spin_release(spin_t *spin){
    xchg(&spin->lock,1);
}

