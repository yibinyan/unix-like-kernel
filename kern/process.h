#ifndef __PROCESS_H_
#define __PROCESS_H_

#include "pagetable.h"
#include "vm.h"

/**
 *	Constant value for process/thread execution
 *	status
 */
#define INIT	    0
#define RUNNABLE	1
#define RUNNING	    2
#define BLOCKING    3
#define ZOMBIE	    4
#define FREE	    5

/**
 * @brief Execution context for one specific 
 * thread/process. Including all registers.
 */
typedef struct context{
	int edi;
	int esi;
	int ebp;
	int ebx;
	int esp;
	int eip;
	
    int eax;
	int ecx;
	int edx;
}ctx_t;

/**
 * @brief Structure to store process related data.
 * Process and thread share the same structure.
 */
typedef struct process{
	int pid;				/* Process/Thread Id*/
	int trap_bottom;
	ctx_t context;			/* Execution context*/
	pagetable_t ptable;		/* Pagetable */
	heap_page_t heaps;		/* Allocated Heap space */
	void *kstack;			/* The kernel stack of the executing process/thread */
	int status;				/* Current thread/process execution status */
	struct process *parent; /* Parent process/thread */
	int waiting;
	int exit_code;			/* Process/Thread exit status code */
	int wait_ticks;
}process_t;

extern process_t* current;
extern process_t* init_process;

void setup_processes();
process_t *alloc_pcb();
process_t *thread_fork(process_t* process);
process_t *fork_process(process_t* process);
void schedule();
int sys_yield(int tid);
int sys_make_runnable(int tid);
int sys_deschedule(int *status);
int sys_wait(int* status);
void sys_vanish();
void sys_set_status(int status);
int sys_yield(int tid);
void refresh_sleeping_ticks();
void sys_sleep(int ticks);
void load_context(process_t *process);


#endif
