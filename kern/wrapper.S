/* Wrapper */

.extern num_ticks
.extern callback_func
.extern int_ack
.extern keyboard_handler
.extern timer_handler
.extern sys_gettid_handler
.extern sys_fork_handler
.extern _set_term_color 
.extern getbytes
.extern sys_exec_handler
.extern sys_wait_handler
.extern sys_set_status_handler
.extern sys_vanish_handler
.extern sys_deschedule_handler
.extern sys_make_runnable_handler
.extern sys_yield_handler
.extern sys_new_pages_handler
.extern sys_remove_pages_handler
.extern get_cursor 
.extern set_cursor 
.extern putbytes
.extern _readline
.extern sys_get_ticks_handler
.extern sys_sleep_handler
.extern sys_thr_fork_handler

.global timer_wrapper
.global keyboard_wrapper
.global sys_gettid_wrapper
.global sys_fork_wrapper
.global sys_halt_wrapper
.global sys_exec_wrapper
.global sys_wait_wrapper
.global sys_set_status_wrapper
.global sys_vanish_wrapper
.global sys_deschedule_wrapper
.global sys_make_runnable_wrapper
.global sys_yield_wrapper
.global sys_new_pages_wrapper
.global sys_remove_pages_wrapper
.global sys_set_term_color_wrapper
.global sys_get_cursor_pos_wrapper
.global sys_set_cursor_pos_wrapper
.global sys_print_wrapper
.global sys_readline_wrapper
.global sys_readfile_wrapper
.global sys_get_ticks_wrapper
.global sys_sleep_wrapper
.global sys_thr_fork_wrapper
.global sys_swexn_wrapper

/* 
 *  This is a wrapper of timer handler, 
 *  it saved general register, and call the callback function
 */
timer_wrapper:
    PUSHA   /*save general register*/

    movl $timer_handler, %eax 
    call %eax           /* call callback function */

    POPA    /* restore general register */
    IRET


/* 
 *  This is a wrapper of keyboard handler, 
 *  it saved general register, and call keyboard handler
 */
keyboard_wrapper:
    PUSHA   /*save general register*/

    movl $keyboard_handler,%eax 
    call %eax           /* call keyboard_handler function */

    POPA    /* restore general register */
    IRET 

/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_gettid_wrapper:
    PUSHA   /*save general register*/

    movl $sys_gettid_handler,%eax 
    call %eax           /* call keyboard_handler function */
    
    mov %eax, 28(%esp) 

    POPA
    IRET

/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_fork_wrapper:
    PUSHA   /*save general register*/

    movl $sys_fork_handler,%eax 
    call %eax           /* call keyboard_handler function */
    
    mov  %eax, 28(%esp) 

    POPA
    IRET

/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_halt_wrapper:
    PUSHA   /*save general register*/

    movl $sys_halt_handler,%eax 
    call %eax           /* call keyboard_handler function */

    POPA
    IRET

/*
 *  This is a wrapper of exec handler,
 *  it saved general register, and call sys_exec_handler
 *  and save tid as return value
 */
sys_exec_wrapper:
    PUSHA   /*save general register*/

    movl $sys_exec_handler,%eax
    push 4(%esi)
    push (%esi)
    call %eax           /* call sys_exec_handler function */

    add $8,%esp

    mov %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of sys_new_pages_handler,
 *  it saved general register, and call sys_new_pages_handler
 *  and save tid as return value
 */
sys_new_pages_wrapper:
    PUSHA   /*save general register*/

    movl $sys_new_pages_handler,%eax
    push 4(%esi)
    push (%esi)
    call %eax           /* call sys_new_pages_handler function */

    add $8,%esp

    mov %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of sys_remove_pages_handler,
 *  it saved general register, and call sys_remove_pages_handler
 *  and save tid as return value
 */
sys_remove_pages_wrapper:
    PUSHA   /*save general register*/

    movl $sys_remove_pages_handler,%eax
    push %esi
    call %eax           /* call sys_remove_pages_handler function */

    add $4,%esp

    mov %eax, 28(%esp)

    POPA
    IRET


/*
 *  This is a wrapper of wait handler,
 *  it saved general register, and call sys_wait_handler
 *  and save tid as return value
 */
sys_wait_wrapper:
    PUSHA   /*save general register*/

    movl $sys_wait_handler,%eax
    push %esi
    call %eax           /* call sys_wait_handler function */

    add $4,%esp

    mov %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of deschedule handler,
 *  it saved general register, and call sys_deschedule_handler
 *  and save tid as return value
 */
sys_deschedule_wrapper:
    PUSHA   /*save general register*/

    movl $sys_deschedule_handler,%eax
    push %esi
    call %eax           /* call sys_deschedule_handler function */

    add $4,%esp

    movl %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of make_runnable handler,
 *  it saved general register, and call sys_make_runnable_handler
 *  and save tid as return value
 */
sys_make_runnable_wrapper:
    PUSHA   /*save general register*/

    movl $sys_make_runnable_handler,%eax
    push %esi
    call %eax           /* call sys_make_runnable_handler function */

    add $4,%esp

    mov %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of sys_yield_handler,
 *  it saved general register, and call sys_yield_handler
 *  and save tid as return value
 */
sys_yield_wrapper:
    PUSHA   /*save general register*/

    movl $sys_yield_handler,%eax
    push %esi
    call %eax           /* call sys_yield_handler function */

    add $4,%esp

    mov %eax, 28(%esp)

    POPA
    IRET


/*
 *  This is a wrapper of wait handler,
 *  it saved general register, and call sys_set_status_handler
 *  and save tid as return value
 */
sys_set_status_wrapper:
    PUSHA   /*save general register*/

    movl $sys_set_status_handler,%eax
    push %esi
    call %eax           /* call keyboard_handler function */

    add $4,%esp

    POPA
    IRET

/*
 *  This is a wrapper of vanish handler,
 *  it saved general register, and call sys_vanish_handler
 *  and save tid as return value
 */
sys_vanish_wrapper:
    PUSHA   /*save general register*/

    movl $sys_vanish_handler,%eax
    call %eax           /* call keyboard_handler function */

    POPA
    IRET

/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_set_term_color_wrapper:
    PUSHA   /*save general register*/

    movl $_set_term_color, %eax 
    push %esi
    call %eax           /* call keyboard_handler function */
    
    ADD $8, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET

/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_get_cursor_pos_wrapper:
    PUSHA   /*save general register*/

    movl $get_cursor, %eax 
    push 4(%esi)
    push (%esi)
    call %eax           /* call keyboard_handler function */
    
    ADD $8, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET


/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_set_cursor_pos_wrapper:
    PUSHA   /*save general register*/

    movl $set_cursor, %eax 
    push 4(%esi)
    push (%esi)
    call %eax           /* call keyboard_handler function */
    
    ADD $8, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET


/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_print_wrapper:
    PUSHA   /*save general register*/

    movl $putbytes, %eax 
    push (%esi)
    push 4(%esi)
    call %eax           /* call keyboard_handler function */
   
    ADD $8, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET


/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_readline_wrapper:
    PUSHA   /*save general register*/

    movl $_readline, %eax 
    push 4(%esi)
    push (%esi)
    call %eax           /* call keyboard_handler function */
   
    ADD $8, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET


/* 
 *  This is a wrapper of gettid handler, 
 *  it saved general register, and call sys_gettid_handler
 *  and save tid as return value
 */
sys_readfile_wrapper:
    PUSHA   /*save general register*/

    movl $getbytes, %eax 
    push 4(%esi)
    push 8(%esi)
    push 12(%esi)
    push (%esi)
    call %eax           /* call keyboard_handler function */
   
    ADD $16, %esp 
    mov %eax, 28(%esp) 

    POPA
    IRET


/*
 *  This is a wrapper of get_ticks_handler,
 *  it saved general register, and call sys_get_ticks_handler
 *  and save tid as return value
 */
sys_get_ticks_wrapper:
    PUSHA   /*save general register*/

    movl $sys_get_ticks_handler,%eax
    call %eax           /* call sys_get_ticks_handler function */

    mov %eax, 28(%esp)

    POPA
    IRET


/*
 *  This is a wrapper of sleep handler,
 *  it saved general register, and call sys_sleep_handler
 *  and save tid as return value
 */
sys_sleep_wrapper:
    PUSHA   /*save general register*/

    movl $sys_sleep_handler,%eax
    push %esi
    call %eax           /* call sys_sleep_handler function */

    add $4,%esp

    movl %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of sys_thread_fork handler,
 *  it saved general register, and call sys_thread_fork_handler
 *  and save tid as return value
 */
sys_thr_fork_wrapper:
    PUSHA   /*save general register*/

    movl $thr_sys_fork_handler,%eax
    call %eax           /* call keyboard_handler function */

    mov  %eax, 28(%esp)

    POPA
    IRET

/*
 *  This is a wrapper of swexn handler,
 *  it saved general register, and call the callback function
 */
sys_swexn_wrapper:
    PUSHA   /*save general register*/
    
    push 12(%esi)
    push 8(%esi)
    push 4(%esi)
    push (%esi)
    
    movl $sys_swexn_handler, %eax
    call %eax           /* call callback function */
    
    add $16,%esp
    movl %eax, 28(%esp)
    
    POPA    /* restore general register */
    IRET


