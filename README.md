# README #

## Project Overview ##    
* Designed and Implemented context switching and mode switching.
* Implemented drivers for the keyboard, the console and the timer.
* Implemented various system calls related to process and thread life cycle, thread management, memory management, console I/O, and miscellaneous system interaction.
* Implement virtual memory system and multitasking.