###########################################################################
#
#    #####          #######         #######         ######            ###
#   #     #            #            #     #         #     #           ###
#   #                  #            #     #         #     #           ###
#    #####             #            #     #         ######             #
#         #            #            #     #         #
#   #     #            #            #     #         #                 ###
#    #####             #            #######         #                 ###
#
#
# Please read the directions in README and in this config.mk carefully.
# Do -N-O-T- just dump things randomly in here until your kernel builds.
# If you do that, you run an excellent chance of turning in something
# which can't be graded.  If you think the build infrastructure is
# somehow restricting you from doing something you need to do, contact
# the course staff--don't just hit it with a hammer and move on.
#
# [Once you've read this message, please edit it out of your config.mk]
# [Once you've read this message, please edit it out of your config.mk]
# [Once you've read this message, please edit it out of your config.mk]
###########################################################################

###########################################################################
# This is the include file for the make file.
# You should have to edit only this file to get things to build.
###########################################################################

###########################################################################
# Tab stops
###########################################################################
# If you use tabstops set to something other than the international
# standard of eight characters, this is your opportunity to inform
# our print scripts.
TABSTOP = 8

###########################################################################
# The method for acquiring project updates.
###########################################################################
# This should be "afs" for any Andrew machine, "web" for non-andrew machines
# and "offline" for machines with no network access.
#
# "offline" is strongly not recommended as you may miss important project
# updates.
#
UPDATE_METHOD = afs

###########################################################################
# WARNING: When we test your code, the two TESTS variables below will be
# blanked.  Your kernel MUST BOOT AND RUN if 410TESTS and STUDENTTESTS
# are blank.  It would be wise for you to test that this works.
###########################################################################

###########################################################################
# Test programs provided by course staff you wish to run
###########################################################################
# A list of the test programs you want compiled in from the 410user/progs
# directory.
#

410TESTS = work deschedule_hang ck1 fork_test1 fork_bomb knife halt_test exec_basic exec_basic_helper fork_wait yield_desc_mkrun exec_nonexist new_pages remove_pages_test1 remove_pages_test2 fork_wait_bomb sleep_test1 wait_getpid loader_test1 cho2 getpid_test1 ack fib fork_exit_bomb print_basic readline_basic cho wild_test1 bistromath minclone_mem mem_permissions make_crash make_crash_helper swexn_basic_test swexn_cookie_monster swexn_regs swexn_stands_for_swextensible  swexn_uninstall_test swexn_dispatch loader_test2 register_test cho_variant

###########################################################################
# Test programs you have written which you wish to run
###########################################################################
# A list of the test programs you want compiled in from the user/progs
# directory.
#
STUDENTTESTS =

###########################################################################
# Data files provided by course staff to build into the RAM disk
###########################################################################
# A list of the data files you want built in from the 410user/files
# directory.
#
410FILES =

###########################################################################
# Data files you have created which you wish to build into the RAM disk
###########################################################################
# A list of the data files you want built in from the user/files
# directory.
#
STUDENTFILES =

###########################################################################
# Object files for your thread library
###########################################################################
THREAD_OBJS = malloc.o panic.o thread_start.o thread.o mutex.o queue.o list.o cond.o xchg.o tqueue.o pause.o sem.o rwlock.o

# Thread Group Library Support.
#
# Since libthrgrp.a depends on your thread library, the "buildable blank
# P3" we give you can't build libthrgrp.a.  Once you install your thread
# library and fix THREAD_OBJS above, uncomment this line to enable building
# libthrgrp.a:

410USER_LIBS_EARLY += libthrgrp.a

###########################################################################
# Object files for your syscall wrappers
###########################################################################

SYSCALL_OBJS = new_pages.o remove_pages.o readfile.o yield.o make_runnable.o deschedule.o readline.o syscall.o gettid.o fork.o halt.o get_cursor_pos.o set_cursor_pos.o set_term_color.o print.o exec.o set_status.o vanish.o wait.o get_ticks.o sleep.o swexn.o

###########################################################################
# Object files for your automatic stack handling
###########################################################################
AUTOSTACK_OBJS = autostack.o pagefault_handler.o

###########################################################################
# Parts of your kernel
###########################################################################
#
# Kernel object files you want included from 410kern/
#
410KERNEL_OBJS = load_helper.o
#
# Kernel object files you provide in from kern/
#
KERNEL_OBJS = kernel.o loader.o malloc_wrappers.o handler.o wrapper.o vm/vm.o vm/pagetable.o asms.o set_seg.o utils/spinlock.o utils/queue.o process.o get_from_stack.o interrupt_handler/keyboard.o interrupt_handler/timer.o syscall_handler/syscall_handler.o syscall_handler/sys_fork.o syscall_handler/sys_gettid.o syscall_handler/sys_halt.o syscall_handler/sys_exec.o syscall_handler/sys_deschedule.o syscall_handler/sys_wait.o syscall_handler/sys_make_runnable.o syscall_handler/sys_yield.o syscall_handler/sys_new_pages.o syscall_handler/sys_remove_pages.o syscall_handler/sys_set_status.o syscall_handler/sys_vanish.o syscall_handler/sys_sleep.o syscall_handler/sys_thr_fork.o syscall_handler/send_instruction.o syscall_handler/sys_get_ticks.o syscall_handler/device_interface/console.o syscall_handler/device_interface/read_keyboard.o utils/set_gates.o set_context.o refresh_context.o schedule_and_switch.o utils/swexn_list.o syscall_handler/sys_swexn.o exception_handler/exception_handler.o exception_handler/exception_wrapper.o  exception_handler/exception_handler_setup.o exception_handler/swexn_handler.o 


###########################################################################
# WARNING: Do not put **test** programs into the REQPROGS variables.  Your
#          kernel will probably not build in the test harness and you will
#          lose points.
###########################################################################

###########################################################################
# Mandatory programs whose source is provided by course staff
###########################################################################
# A list of the programs in 410user/progs which are provided in source
# form and NECESSARY FOR THE KERNEL TO RUN.
#
# The shell is a really good thing to keep here.  Don't delete idle
# or init unless you are writing your own, and don't do that unless
# you have a really good reason to do so.
#
410REQPROGS = idle init shell

###########################################################################
# Mandatory programs whose source is provided by you
###########################################################################
# A list of the programs in user/progs which are provided in source
# form and NECESSARY FOR THE KERNEL TO RUN.
#
# Leave this blank unless you are writing custom init/idle/shell programs
# (not generally recommended).  If you use STUDENTREQPROGS so you can
# temporarily run a special debugging version of init/idle/shell, you
# need to be very sure you blank STUDENTREQPROGS before turning your
# kernel in, or else your tweaked version will run and the test harness
# won't.
#
STUDENTREQPROGS =
