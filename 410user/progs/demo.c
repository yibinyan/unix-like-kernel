/** @file 410user/progs/getpid_test1.c
 *  @author zra
 *  @brief Tests gettid().
 *  @public yes
 *  @for p2 p3
 *  @covers gettid
 *  @status done
 */

/* Includes */
#include <simics.h>    /* for lprintf */
#include <syscall.h>
#include <string.h>
#include <stdlib.h>

/* Main */
int main() {

    /*
    int tid = fork();
	    char buf[256];

    if (tid > 0){
        lprintf("Parent");
        //lprintf("my pid is demo, tid = %d", tid);
        readline(256, buf);
    }else if (tid == 0) {
        lprintf("Child!");
        while(1);
    }
    */

/*
    int index = 0;
    while (1) {
       if(index%9999999==0){
          lprintf("This is demo");
          index = 0;
        }
     index++;
    }
*/

    halt();
    //swexn(NULL,NULL,NULL,NULL);

/*
    char buf[500];

    int len = readfile(".", buf, 500, 0);
    int templen;
    int i = 0;
    
    for (i = 0; i < len; i++){
        templen = strlen(&buf[i]);
        lprintf("%s", &buf[i]);
        i = i + templen;
    } 
        
    while (1);
*/
    //lprintf("my pid is demo");
	return 0;
}
